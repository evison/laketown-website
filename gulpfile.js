const { series, parallel, watch } = require("gulp");
const { reload } = require("./gulp/tasks/reload");
const { copy } = require("./gulp/tasks/copy");
const { hugo } = require("./gulp/tasks/hugo");
const { svg, svgClean } = require("./gulp/tasks/svg");
const { images, imagesClean } = require("./gulp/tasks/images");
const { scripts, scriptsClean } = require("./gulp/tasks/scripts");
const { styles, stylesClean } = require("./gulp/tasks/styles");
const { clean } = require("./gulp/tasks/clean");
const { cachebust } = require("./gulp/tasks/cachebust");
const { dato } = require("./gulp/tasks/dato");
const cfg = require("./gulp/config");
const argv = require("yargs").boolean("p").argv;
const gutil = require("gulp-util");
const browserSync = cfg.browserSync.instance;
const pkg = require("./package.json");

const isProduction = argv.p;
gutil.log(gutil.colors.bold(`ℹ  ${pkg.name} v${pkg.version}`));

if (isProduction) {
  gutil.log(gutil.colors.bold.green("🚚  Production Mode"));
} else {
  gutil.log(gutil.colors.bold.green("🔧  Development Mode"));
}

/*-----------------------------------------------------------------------------
  Watch files
------------------------------------------------------------------------------*/
function watchfiles(cb) {
  const settings = cfg.browserSync.settings;
  const settingsWithMiddleware = settings;

  settings.server = Object.assign({}, settings.server, {
    middleware: isProduction ? [compress()] : [],
  });

  browserSync.init(settingsWithMiddleware);

  cfg.copy.bundles.forEach((bundle) => {
    watch(bundle.sourcePath, series(copy, reload));
  });

  watch(cfg.dato.watch, series(dato));
  watch(cfg.hugo.watch, series(hugo, reload));
  watch(cfg.images.sourcePath, series(imagesClean, images, reload));
  watch(cfg.scripts.components, series(scripts));
  watch(cfg.scripts.bundles, series(scripts));
  watch(cfg.styles.watch, series(styles));
  watch(cfg.svg.sourcePath, series(svgClean, svg, reload));
  cb();
}

exports.build = series(
  clean,
  copy,
  styles,
  dato,
  hugo,
  scripts,
  images,
  svg,
  cachebust
);

exports.default = exports.dev = series(
  clean,
  dato,
  styles,
  parallel(images, svg),
  parallel(copy, scripts),
  hugo,
  cachebust,
  watchfiles
);
