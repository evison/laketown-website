---
title: CAMPGROUND POLICIES
type: page
layout: page-no-hero
hero_image: ''
hero_heading: CAMPGROUND POLICIES
hero_map: null
contact_form: null
contact_heading: Contact Form
contact_body: ''
faq:
  questions: []
---

<div><img src="/static/images/laketown-ranch-campgrounds.svg" alt="Laketown Ranch Campgrounds" style="display:block;margin:auto;" /></div>

<h1 class="a-heading--underlined">Campground Policies</h1>

**OUR POLICIES ARE DESIGNED TO ENSURE THAT EVERYONE HAS A SAFE, FUN AND FAIR EXPERIENCE AT LAKETOWN RANCH CAMPGROUNDS - GUESTS, STAFF AND WILDLIFE ALIKE!**

### Reservations and Occupancy

- Prepayment of the entire balance is required at the time of booking.
- Any cancellations received within 7 days of the scheduled arrival will result in a forfeiture of your entire payment.
- Cancellations received before this 7 day period are subject to a $35 cancellation fee, the remainder is to be refunded to the credit card on file.
- Any changes to your reservation must be received 7 days before arrival
  If you wish to extend your stay, and your site is available, please notify our office before 11:00 a.m. on your scheduled date of departure.
- Occupancy rules for REGULAR SITES: - Prices are based on 4 adults per site. Additional adults fee applicable. - Maximum 6 adults per site. - Children under 16 years old are allowed at no additional charge - Max number of occupants may not exceed 8 people per site, including children - A camping party can have one vehicle and trailer. Either one (but
  not both) may be an RV. - In addition to vehicles, two tents are permitted per site.
- Occupancy rules for GROUP SITES:
  - Prices are based on 16 adults. Additional adults fee applicable.
  - Max 20 adults per site.
  - Children under 16 years old are allowed at no additional charge
  - Max number of occupants may not exceed 32 people per site including children.
  - A group can have 5 vehicles and trailers. Maximum 5 steering wheels.
  - In addition to vehicles, four tents are permitted per site.
- Additional parking passes can be purchased for $10. Additional vehicle parking is within walking distance of all sites.
- Motorhomes towing a vehicle will be considered one steering wheel

### Cancellations

- Any cancellations received within 7 days of the scheduled arrival will result in a forfeiture of your entire payment. Cancellations received before this 7 day period are subject to a $35 cancellation fee, the remainder is to be refunded to the credit card on file.
- In the case of any no-shows or early departures, guests will still be responsible for the full amount of their reservation.
- Cancellations will only be accepted by email to [campground@laketownevents.com](mailto:campground@laketownevents.com)
- We do not give credit or refunds due to inclement weather. We enjoy rain and shine!

### Vehicles and Boats

- All vehicles and boats must be registered at the office. A valid parking pass should be on display at all times.
- We are a family campground. Kids are at play throughout our property. Therefore, a speed limit of 10km/h (6mph) within the camping areas is enforced.
- No cruising or excessive trips throughout the site are permitted.
- Parking for the above mentioned number of vehicles is included in your campsite rental. Additional parking passes can be purchased for $10.
- Onsite boat parking is available for $10/day.
- Keep roadways clear at all times. Vehicles impeding roadways will be towed.
- Washing of vehicles on the property is not permitted.
- Use of personal motorized UTV’s, dirtbikes, or ATVs is prohibited on Laketown Ranch property. Pedal bikes are permitted and encouraged.

### Campers

- Campsite permit holders must be at least 18 years of age.
- The permit holder is responsible for the conduct and behaviour of their family and other guests.
- Check in time is 2:00 p.m. Check out time is 11:00 a.m.
- If you wish to change campsites, the change must be cleared with the office prior to occupying the site.
- We reserve the right to refuse entry to any person or animal without notice or reason.
- We reserve the right to refuse service to anyone for violating campground rules, destructive behaviour or creating a disturbance. To protect our campers, trespassers will be prosecuted.
- Property gates will be closed from 11pm - 7am on weekdays, and 12am - 7am on weekends.

### Campground Policies

- This is a family campground. Behave in a respectful manner at all times.
- Amplified music must be off between the hours of 10:00 p.m. and 9:00 a.m. Loud music is not permitted at any time. Speakers larger than a toaster are not permitted.
- Quiet time is 11:00 p.m. to 8:00 a.m. on weekdays and 12:00 a.m. to 8:00 a.m. on weekends. Excessive noise is not permitted at any time.
- Generators must be off between 11:00pm and 8:00am.
- Climbing on any structures, trees, RV’s (with the exception of repairs), fences, etc., may lead to eviction from grounds.
- At no time may unauthorized persons enter areas designated as out of bounds.
- Anyone who fails to comply with campground regulations will be required to vacate the property immediately. No refunds will be provided.
- Campers who do not demonstrate care for themselves, others or the environment will be told to leave the campground and will have their camping passes revoked without a refund.
- Any after-hours emergencies can be referred to our onsite caretaker. In the case of an acute health emergency, please call 911.
- Laketown Ranch is not responsible for any loss of or damage to any personal or private property. Please keep all valuables stored and locked away.
- No glass bottles are permitted in the campgrounds.
- Laketown Ranch reserves the right to charge a $200 clean up fee for all sites left containing excessive garbage, equipment, damage, human and/or animal waste or anything else requiring staff intervention. Furthermore, in cases such as these guests may be banned from Laketown Ranch for camping and all future events.

### Grounds for Ejection

- Invalid, tampered with, fraudulent or absent camping pass
- Failure to keep a clean campsite will result in eviction from grounds. No household furniture permitted.
- Rowdy or disrespectful behaviour
- Excessive noise/noise after curfew
- Excessive garbage in campsite
- Sales of any unauthorized or illegal materials
- Possession of fireworks, weapons or illegal drugs
- Contributing to underage drinking
- Speeding on roadways
- Public intoxication

Ejection from grounds will result in being banned from Laketown Ranch for all future camping and events.

### Visitors

- Visitors are permitted. All visitors must check in first with the office before entering grounds.
- Visitor parking pass is required and is available via the office check in.
- Fee may be applicable for access to onsite activities and entertainment.
- Access to onsite activities and entertainment subject to capacity.
- All visitors must be off site by 11pm on weekdays, and 12am on weekends

### Natural surroundings

- Cutting or chopping trees on or around Laketown Ranch is prohibited, as is the burning of any property or materials.
- Our ranch is home to many animals. At no time should anyone approach, harass or feed any of these animals. Be vigilant at all times - these are wild animals.
- Do not connect rain covers or tie anything else to bushes, campground infrastructure, or trees. All rain covers must be free-standing and secured to the ground.
- Do not forage for wood or damage the bushes or other flora.

Take only photos, leave only footprints!

### Fires

- Cook stoves, propane fire pits and BBQ’s MUST be 12 in (30 cm) off the ground at all times.
- No wood fire pits or charcoal cooking devices are allowed.
- You must have a fire extinguisher at your campsite if you are using a cook stove, BBQ or propane fire pit.

### Pets

- Friendly pets with up-to-date vaccinations are welcome.
- Campers bringing pets are subject to a one-time fee of $20 per pet. Limit of two pets per campsite.
- Because of wild animals in the area of the ranch, pets must be kept on leash at all times while on our property.
- You are responsible for picking up after your pooch.
- Do not leave your pets unattended.
- Cats and all other pets must remain inside your vehicle.
- Exotic pets are not permitted.

### Waste management

- Laketown Ranch is a pristine, natural environment that is home to herds of elk, transiting bears and other wildlife. Please help us keep it clean by utilizing the proper waste and recycling bins throughout the site.
- Any food or garbage odor in campsites is a bear attractant, and therefore poses a danger to all campers.
- Campsites must be kept clean and free of food scraps. We have bears for neighbours, and unfortunately a fed bear is a dead bear.
- To ensure our property is safe for both guests and local wildlife, our staff will remove any food or garbage that is left unattended and/or is not stored in a vehicle or other safe location.
