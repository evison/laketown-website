---
title: Parking
type: page
layout: page
hero_image: 'https://www.datocms-assets.com/64219/1699737217-hero-parking.jpg'
hero_heading: Parking
hero_map: false
contact_form: false
contact_heading: Contact Form
contact_body: ''
faq:
  questions: []
---

#### Parking and Transportation

Day parking is available on-site. The day parking rate for a standard-sized vehicle varies per event and is payable upon arrival in the lower parking lot. Vehicles left overnight must be moved by 11am the next day, or be charged an extra day.

Weekend parking passes are also available for events on their respective websites.

Other transportation options are:

* Carpool - Any vehicle arriving with 5 or more guests, all in seat belts, can park for FREE!
* Shuttles services

For local driving:

* No parking on Highway 18 or any of these roads: Youbou, Indian, North Shore or Teleglobe. Vehicles will be towed at owner’s expense.
* Local traffic only on Indian Rd and North Shore Rd.
* Speed limit reduced to 50 km/h on Youbou Rd.


#### Drinking & Driving

ALWAYS arrange a safe ride home. Taxis are available on site and we have a comprehensive shuttle service. Road checks will be done nightly by local law enforcement.

{{< disclaimer >}}