---
title: Festivals & Concerts
type: page
layout: page
hero_image: 'https://www.datocms-assets.com/64219/1699736848-hero-rent-the-venue.jpg'
hero_heading: Festivals & Concerts
hero_map: false
contact_form: true
contact_heading: Venue Inquiry
contact_body: Want to host your event at Laketown Ranch?
faq:
  questions: []
---

Laketown Ranch is a 250 acre modern, multi-use entertainment complex and campground in the heart of Vancouver Island, British Columbia.

Whether you’re hosting a one-night event or a multi-day festival, Laketown Ranch has the venue and support that you’ll need for a successful event. With amenities such as flush toilets, showers, wifi, food preparation areas, onsite equipment (tents, golf carts) and much more, we can cater your experience to suit your needs.

## Stages

- <h3>Main Stage</h3>
  - Structurally Engineered Permanent Stage
  - 60ft x 56ft Main Deck
  - 16ft x 32ft Stage Wings – 50 person per deck second level viewing
  - 50ft of Video Bays (25ft per side)
  - Total width: 140ft, Total height: 50ft
  - 30ft stage thrust removable at renter’s expense
  - 16000 person viewing capacity
- <h3>Lakenight Stage</h3>
  - Permanent 40ft x 20ft stage deck with 40ft saddle tent cover
  - 4000 person viewing capacity
- <h3>Flats Stage</h3>
  - _Western Barn_ style stage with 20ft x 16ft deck
  - 2000 person viewing capacity
- <h3>Hanner Stage</h3>
  - 20x16 stage in grassy area in upper camping area
  - 800 person viewing capacity

## Location

It’s convenient for fans and artists to reach Laketown Ranch. We are located 60 miles north of
Victoria, British Columbia, 100 miles from Vancouver and 200 miles from Seattle.

There are 4 international airports within 2 hours drive, as well as multiple BC ferry terminals and
seaplane hubs.

To find out more please [admin@laketownevents.com](mailto:admin@laketownevents.com)
