---
title: Festival Info
type: page
layout: info-faq
hero_image: 'https://www.datocms-assets.com/64219/1699733759-hero-visitor-info.jpg'
hero_heading: Festival Info
hero_map: false
contact_form: true
contact_heading: Contact Form
contact_body: Didn't find what you were looking for?
faq:
  questions:
    - question: Can I bring food and drinks onto the grounds?
      answer: |-
        * Personal-sized coolers for food are permitted.
        * No outside liquids of any kind.
        * Potable water is available. Feel free to bring a refillable drink container (no glass or metal) to use at our water filling station.

        For the safety of police, security, and our guests, all bags and belongings are subject to search.
    - question: Can I use my credit or debit card on-site?
      answer: You can use your credit and debit cards at the ATMs located on-site. Unfortunately vendors cannot take credit or debit cards.
    - question: Will I be able to use my cell phone on site?
      answer: Yes. There is cell reception on-site.
    - question: Is day parking available?
      answer: 'Yes, day parking is available on-site. The day parking rate for a standard-sized vehicle varies per event and is payable upon arrival in the lower parking lot.'
    - question: Do you provide free parking for carpools?
      answer: 'Yes. Any vehicle arriving with 5 or more guests, all in seat belts, can park for FREE!'
    - question: Can I bring an ATV or personal vehicle?
      answer: 'No Personal ATVs, dirt bikes, golf carts, horses or similar vehicles are permitted. Mobility Scooters can be used by those with a disability.'
    - question: Can I bring a pet or other animal?
      answer: 'No pets allowed on site, with the exception of registered service dogs. If you have a service dog you must provide to Guest Services the BC Guide & Service dog registration.'
    - question: Is there a Lost & Found?
      answer: |-
        Head over to the Guest Services building, where our Lost & Found is located. We have great volunteers there who will do their best to reunite you with anything you’ve lost. Please secure your valuables throughout the weekend.


        Post event, items are transferred to the Wideglide Entertainment Ltd. office in Duncan. Please email [info@laketownevents.com](mailto:info@laketownevents.com) for information.
---

{{< h2-underlined Info >}}


#### Security, Health Services, Lost & Found
Security and Festival Health Services are available 24 hours a day during festivals, and are located behind the Flats Stage. Look for the medical sign on top of the flag pole.

Lost & Found is located at Guest Services in Laketown Flats. After the event, please contact [info@laketownevents.com](mailto:info@laketownevents.com) for lost items.

#### Washrooms
There are many flush toilets located throughout Laketown Ranch including Laketown Flats, VIP, Festival Bowl and Camping. Portable washrooms are located throughout the festival grounds, in the concert bowl and in camping areas.

#### Cash Machines
ATM’s are located at the ticket gate and beverage gardens. Locations subject to change without notice. Service fees apply.

#### Cameras
Due to artist contracts, professional recording equipment will not be permitted without press credentials. Video cameras and cameras with detachable lenses are prohibited inside the festival grounds and are subject to confiscation.

#### Recycling & Garbage
LEAVE NO TRACE! Laketown Ranch is a pristine, natural environment that is home to herds of elk and other wildlife. Please help us keep it clean by utilizing the proper waste and recycling bins throughout the festival site. Garbage drop-off locations are located throughout camping. There is no curbside pickup. Please be responsible and use the bins provided.

Clean campsites will be rewarded. Dirty ones may be ejected at any time. Please treat Laketown Ranch as your own home and respect it.

Again, LEAVE NO TRACE!

#### Alcohol
Outside alcohol in cans and plastic containers are permitted in the campground only.

Open alcohol is not permitted on roadways and will be confiscated and disposed of.

Visibly intoxicated persons will not be permitted to enter the concert bowl.

ABSOLUTELY NO UNDERAGE DRINKING OR CONTRIBUTING TO MINOR DRINKING. Anyone found in violation of this will be reported to security and will face ejection from site.

#### Transportation & Roadways
The roadway speed limit is 10 km/hr. Excessive speed causes dust and is dangerous. Those who drive over the speed limit will be subject to ejection. No cruising or excessive trips throughout the site are permitted.

No personal ATV’s, dirt bikes, bicycles or golf carts are permitted.

Keep roadways clear at all times. Vehicles impeding roadways will be towed.

Always arrange a safe ride home. Taxis are available on site and we have a comprehensive shuttle service. Road checks will be done nightly by local law enforcement.

#### Safety
If you notice anything that puts you or others in danger, please notify security immediately.

Children must be supervised by parents/guardians at all times.

Upon close of the concert venue, be prepared for a rush to the campsites and parking. No gate breaks.

Weapons, firearms, pyrotechnics and any other dangerous items that could pose harm or hazard are strictly prohibited. Dangerous materials will be confiscated and the persons will be subject to site ejection and possible criminal charges.

Any violent or disruptive behaviour will result in immediate eviction from grounds and possible persecution.

Flying colours, patches, or any type of gang-related paraphernalia will not be tolerated.

Racist, sexist, homophobic or excessively lewd paraphernalia will not be tolerated.

In the event of an emergency evacuation, proceed calmly to the nearest exit and follow event staff to a pre-determined muster station. Never attempt to go against the pedestrian flow. Laketown Ranch staff will provide proper assistance for disabled patrons and children during an emergency evacuation.

#### Grounds For Ejection

* Invalid, tampered with, fraudulent or absent wristband (civil and/or criminal charges will be sought)
* Rowdy or disrespectful behaviour
* Excessive noise/noise after curfew
* Excessive garbage in campsite
* Sales of any unauthorized or illegal materials
* Possession of fireworks, weapons or illegal drugs
* Contributing to underage drinking
* Speeding on roadways
* Public intoxication

Be respectful of yourself, other guests, staff, volunteers, vendors, security, RCMP, and the beautiful natural environment around you.

#### Top 5 Things To Keep In Mind At Laketown Ranch

1. Take care of yourself and others. Wear a hat, sunscreen, sleep, do not over consume alcohol, wear proper footwear, stay hydrated and ask for help. We care about you.
2. LEAVE NO TRACE! Laketown Ranch is a pristine, natural environment that is home to herds of elk and other wildlife. Please pick up after yourself, use proper waste, recycling and compost bins provided, and encourage others to do the same. Leave No Trace!
3. Hydrate yourself! We have ample water stations throughout the site. Our water comes from a natural aquifer located directly under Laketown Ranch. We are adhering to current water restrictions, because environmental stewardship is key!
4. NO WOOD FIRES! No wood fire pits, charcoal cooking devices or tiki torches are permitted on site. Please keep propane firepits/stoves and BBQ’s 12 in (30 cm) off the ground. Have a fire extinguisher on site at your camp. **Subject to provincial or local fire restrictions.
5. Your safety is paramount. If you see anything suspicious, violent or illegal, contact our Security anytime. Help us help you!

{{< disclaimer >}}