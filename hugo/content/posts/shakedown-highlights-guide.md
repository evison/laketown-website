---
title: Shakedown Highlights & Guide | Laketown Ranch
type: post
layout: post
card_image: 'https://www.datocms-assets.com/64219/1655890588-laketownshakedown_fb_1920x1005_everlastpresents_03-edit.jpg?w=1260&h=630&q=50&fit=crop&fm=webp'
hero_image: 'https://www.datocms-assets.com/64219/1655890588-laketownshakedown_fb_1920x1005_everlastpresents_03-edit.jpg?w=1920&h=1080&q=60&fit=crop&fm=webp'
hero_heading: Shakedown Highlights & Guide
excerpt: Shakedown Highlights & Guide
slug: shakedown-highlights-guide
card_tag: Laketown Shakedown 2022
date: 2022-06-22T00:00:00.000Z
share_title: Shakedown Highlights & Guide
share_url: 'https://laketownranch.com/posts/shakedown-highlights-guide'
share_description: What to Expect at Laketown Shakedown 2022
share_image: 'https://www.datocms-assets.com/64219/1655890588-laketownshakedown_fb_1920x1005_everlastpresents_03-edit.jpg?w=1200&h=630&q=60&fit=crop&fm=webp'
---

Ranchin' During Laketown Shakedown….

# HANNER STAGE
Our newest and coolest stage will run Friday & Saturday and is located in our upper camping area. This area isn’t licensed, meaning guests are encouraged to come (with their own accouterments!) to take in the good vibes, food and shade offered by our stretch tent.

Featuring our Hilltop Food Stop and **brunch jams between 10:30am - 1:00pm**, guests are encouraged to grab some grub, take in some tunes and hang out while snacking on some good food.

On **Friday, July 1** from **1:00pm to 4:00pm** we feature our **ALL GOOD AFTERNOON** with [DJ All Good](https://soundcloud.com/peter-poole-1) and friends. We’ll also have a live mural being painted by a very special guest. Don’t miss it!

![](https://www.datocms-assets.com/64219/1655882392-agnew1-copy-3-2.jpg)

![](https://www.datocms-assets.com/64219/1655885903-laketownshakedownjune2019-c-annemariesphotography_bcphotographer-99.jpg)

And who doesn’t LOVE a little **AFTERNOON DELIGHT!** Well we’ve got just what you want on **Saturday, July 2** between **1:00pm to 4:00pm** when Astrocolor’s front man [Neil Jam3s](https://soundcloud.com/soundofbliss) hits the decks with some special guests. Oh what a time we’ll have!

You MUST make it up the hill!

# LAKENIGHT STAGE

![](https://www.datocms-assets.com/64219/1653979275-5d4_3366-edit.jpg)


**Thursday, June 30**

Opening up our new and improved stage will be local artist [Nokturnal Funk](https://soundcloud.com/nokturnalfunk) with his soul-soothing funky tunes before the legend [GI Blunt](https://soundcloud.com/g-i-blunt) gets the party rockin'.

![](https://www.datocms-assets.com/64219/1655882571-94491311_2806343072796677_7559075177477177344_n-edit.jpg)

At **10:15**, Victoria hip-hop up and comer [Brainiac](https://soundcloud.com/thamainiac) is not to be missed. The always incredible [Longwalkshortdock](https://soundcloud.com/longwalkshortdock) with work the crowd into a dance frenzy before the smooth hip hop vibes of [Def3](https://open.spotify.com/artist/6kqN8RoL2VpRM8EgGwHiHG) close out the first night. What a way to kick things off!

**Friday, July 1**

Kicking the day off with the smooth latin vibes is [La Chuparosa](https://soundcloud.com/battery-poacher). This set comes highly recommended! 

![](https://www.datocms-assets.com/64219/1655882751-la-chup-promo-pic-1-edit.jpg)

Following them, the highly anticipated set by [Kimmortal](https://open.spotify.com/artist/0ioYxkShKhYOKpQh0ERVFS) takes off. This is a chance to see why their song caught the attention of big namers such as Alexandria Ocasio-Cortez. 

![](https://www.datocms-assets.com/64219/1655882836-kimmortal-photo-landscape-edit.jpg)

Party rockers [Case of the Mondays](https://open.spotify.com/artist/0TcmYfWCoJWqMBHWmLr3LL?autoplay=true) will keep things hopping before taking a break for the mainstage shows. 

At **10:15** [DJ Anger](https://soundcloud.com/dee-jay-anger) will fire things back up before keytar warrior, and block rocker [K+Lab](https://soundcloud.com/klabnz) takes us to another dimension. Closing things out is local legend [Murge](https://soundcloud.com/djmurge)! “How ya’ll feeling out there!?”. Pretty awesome!

**Saturday July 2**

Combining Hip-Hop, Electronic, Latin & World Music, [Nostic and Nicki](https://open.spotify.com/artist/3wx4mwfYLHaCEcxlffqb5B) will open up the stage for our final day of programming. This set is also highly recommended!

![](https://www.datocms-assets.com/64219/1655882921-missyd_1_bydavidmarkweiproductions-edit.jpg)

Hip Hop/ Rap /Soul artist and bilingual FEMCEE [Missy D](https://soundcloud.com/themissyd) is not to be missed when she takes the stage at **5:30pm**. Pakistani-Canadian artist [Khanvict](https://soundcloud.com/khanvictmusic) will show us all why he is turning heads around the world with his culture-jamming beat science. 

![](https://www.datocms-assets.com/64219/1655883033-dj-khanvict_0777-1_1-edit.jpg)

Local hip-hoppers [Illvis Freshly](https://soundcloud.com/illvisfreshly) take the stage at **10:15pm** before the always amazing [Astrocolor](https://open.spotify.com/artist/4OvLy4oD9IFQ8YCbbMDVRD) shows us all what a good time is all about. Closing out the stage for the weekend will be Funk Hunters member and the ALWAYS a good time, [Dunks](https://soundcloud.com/dunks).

# MAIN STAGE

![](https://www.datocms-assets.com/64219/1653975282-sunday-laketown-shakedown-colin-smith-takes-pics-2019-123.jpg)

**Thursday, June 30**

The first party in our main concert bowl in three years (THREE YEARS!) kicks off when **gates open at 3pm**. 

BC rockers [daysormay](https://open.spotify.com/artist/1gneO1Mf6DCsgxUtDzF4lS) will kick things off right with their energetic live performance. Ghanaian born R&B alt-pop artist [Mauvey’s](https://soundcloud.com/lovemauvey) set is not to be missed and local legend [Vince Vaccaro](https://soundcloud.com/user-332010619) will be sure to bring a lot of nostalgia and good energy to the stage. 

![](https://www.datocms-assets.com/64219/1655883264-photo-2nd-choice-mauvey-2022_credit-bree-ross-layrea-edit.jpg)

![](https://www.datocms-assets.com/64219/1655883358-hm-promo-2021-9-col-yung-yemi.jpg)

Polaris Prize winner [Haviah Mighty](https://open.spotify.com/artist/3UROQ34SGxV7h71Z3Gqp8u) will bring her hip-hop talents to the stage before back-to-back-to-back Juno winners take the stage - [Lights](https://open.spotify.com/artist/5pdyjBIaY5o1yOyexGIUc6) followed by Canadian icons [Arkells](https://open.spotify.com/artist/3ShGiAyhxI6Rq3TknZ3gfk?si=VgET_3kaQr20lHWTKLkbDQ&nd=1). 

This year we’re bringing the after party to the main stage each night to keep the good times rolling on where the production is gob-smacking. **Thursday** we will feature [Felix Cartal](https://open.spotify.com/artist/6roDXEmZ6AARdOUv6x5U2v), who will hit the stage right after Arkells. 

**Friday, July 1**

At 3pm, the local [Quw’utsun’ Tzinquaw Dancers](https://www.youtube.com/watch?v=LL7xJQ6U2Yo&ab_channel=SAGAcomProductions) will open the main stage with a powerful performance before the uber-talented [Diamond Cafe](https://soundcloud.com/tristanlovesdiamonds) gets the crowd doing their own dance of joy. 

![](https://www.datocms-assets.com/64219/1655883426-img_6559-edit-2.jpg)

Adored by many in this region, New York born [R.O. Shapiro](https://soundcloud.com/roshapiro) is going to wow the crowd with his soulful voice and good vibes. This performance is not to be missed. 

![](https://www.datocms-assets.com/64219/1655883501-r-o-laughing-guitar-edit.jpg)

Vancouver’s [Dear Rouge](https://open.spotify.com/artist/0YkjOpIntNmlG1PNF2dqSy) will show us all why they are the perfect act for a sunny evening in July before legendary California rockers [Sugar Ray](https://open.spotify.com/artist/4uN3DsfENc7dp0OLO0FEIb) take us back with hits such as Every Morning and Fly. 

The energy will be amazing as festival headliners [Wu-Tang Clan](https://open.spotify.com/artist/34EP7KEpOjXcM2TCat1ISk) hit the stage for their first ever performance on Vancouver Island as a full group. 

![](https://www.datocms-assets.com/64219/1655885716-wutang10229rt-edit.jpg)

Friday’s mainstage afterparty will feature international stars [Goldfish](https://soundcloud.com/goldfishlive) and their infectiously happy dance music. 

**Saturday, July 2**

[Old Soul Rebel](https://open.spotify.com/artist/6I3WzI96mccqCYFasXQYgB) hits the stage shortly after gates to kick the day off with some soulful rock before surf-rock legend [Daniel Wesley](https://open.spotify.com/artist/1KTeOdeSoRAIv8xSpATVyZ) gets the crowd feeling the good vibes with his arsenal of great songs. 

Vancouver-based, horn-powered, percussion-fueled sonic and visual assault group [Five Alarm Funk](https://open.spotify.com/artist/34BHw3ZxeQsZ2yYxoummp7?autoplay=true) takes the stage before Rock Album of the year award winner from the 2021 Junos, [JJ Wilde](https://open.spotify.com/artist/1pLZeUSXJwVVJSPhmBSwf6). It’s going to be a party!

[The Glorious Sons](https://open.spotify.com/artist/5CPxrqCStgt6AfI4fLiedH?si=m34Nw6eSQYaMtSvTLkaCng&nd=1) will rock our faces off and show us why they are one of the best and most sought after bands in Canada at the moment. 

Saturday’s afterparty, will feature [The Halluci Nation](https://open.spotify.com/artist/2jlWF9ltd8UtoaqW0PxY4z?si=cJp5MWK8SWCQw3nY0FIq4Q&nd=1) which is not a set to be missed. The buzz is already building in anticipation for this set by the group formerly known as **Tribe Called Red**. 

![](https://www.datocms-assets.com/64219/1655883576-the-halluci-nation-3-web-photo-credit_remi-theriault-edit.jpg)

# FLATS STAGE

Running on **Friday and Saturday** in the Laketown Flats, this is the stage to catch some of the regions best up and comers and tomorrow next stars. 

Under the shade of our bigtop, Friday we will feature rockers [Faultline](https://open.spotify.com/artist/5iHKhfDnBhVdFICFX8Fwip?si=XQBJOPhaSIuWYPr-Sf5CnA&utm_source=copy-link&nd=1), Nanaimo’s own [The Odd Neighborhood](https://open.spotify.com/artist/09UG0CDaidW9diJSE3rAOa?nd=1) and a band already making big waves in the industry, [Bankes Brothers](https://www.youtube.com/channel/UCGV8Gn1J7zXQYJB23xYxksA). 

Saturday opens up with the smooth jazzy vibes of [hard.times with Lady Phyl](https://www.instagram.com/hard.times.music/). 604 Records recording artist [Michaela Slinger](https://open.spotify.com/artist/59QPoeNCHdaVDlFxw7ZDVQ?si=195xr3O5Rd-qmMyBcbwYew&nd=1) is not to be missed and she shows us why she’s a rising star. 604 Records label mates [Vox Rea](https://open.spotify.com/artist/4SRrbYd7KBkGdOCAekSnY1?autoplay=true) will close out the stage for the weekend with their euphoric sounds.

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/playlist/417I8R692prZs5D80XXFrG?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

![](https://www.datocms-assets.com/64219/1655889770-sd2022-schedule-all-1200.jpg)