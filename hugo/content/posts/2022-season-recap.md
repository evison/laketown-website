---
title: 2022 Season Recap! | Laketown Ranch
type: post
layout: post
card_image: 'https://www.datocms-assets.com/64219/1661323422-tfalkchalmers-348-1200.jpg?w=1260&h=630&q=50&fit=crop&fm=webp'
hero_image: 'https://www.datocms-assets.com/64219/1661323422-tfalkchalmers-348-1200.jpg?w=1920&h=1080&q=60&fit=crop&fm=webp'
hero_heading: 2022 Season Recap!
excerpt: What an amazing 2022 season!
slug: 2022-season-recap
card_tag: Laketown Ranch 2022
date: 2022-08-23T00:00:00.000Z
share_title: null
share_url: 'https://laketownranch.com/posts/2022-season-recap'
share_description: null
share_image: 'https://www.datocms-assets.com/64219/1661323422-tfalkchalmers-348-1200.jpg?w=1200&h=630&q=60&fit=crop&fm=webp'
---

# We are back!
What an incredible return to the Ranch it has been. The 2022 festivals were far more than we could have imagined. 

After 3 years off, we were eager to have you all back, and showcase the upgrades and improvements we’d made to the site. But more than anything, we were extremely excited to witness all the smiles and great memories being created once again. 
Hands were sore from high-fives, and the backs of heads hurt from smiling. It was the perfect return. You all did great. So we thank you all for being so respectful and awesome.

A huge thank you to all of our volunteers, crew, sponsors and artists for helping to make it happen. We truly could not have done it without you.


# Laketown Shakedown
**Jun 30 - Jul 2**


Well, after many promises, it finally happened - Wu-Tang Clan played on our main stage for thousands of island revelers who have been waiting for years.

Kicking off the season for three fun-packed days and nights, the Shakedown set the tone (and benchmark) for summer festivals on the island. The weather was perfect, the crowd was awesome, and the music was incredible.

Thursday saw acts [Mauvey](https://www.mauvey.com/), [Haviah Mighty](https://haviahmighty.ca/), [Lights](https://www.iamlights.com/), and [Arkells](http://www.arkellsmusic.com/) dust off the main stage, alongside local talent [Vince Vaccaro](http://vincevaccaro.com/) & [Daysormay](http://daysormay.com/), and Vancouver's [Felix Cartal](https://www.felixcartal.com/).
The brand new Lakenight Stage saw its first performances by [Nokturnal Funk](https://soundcloud.com/nokturnalfunk), [GI Blunt](https://soundcloud.com/g-i-blunt),
[BRAINiac with Ruze Records](https://soundcloud.com/thamainiac), [Longwalkshortdock](https://soundcloud.com/longwalkshortdock), and [Def3](https://def3.ca/).
Friday picked the pace up with the addition of performances by great local talent on the Laketown Flats Stage, as well as brunch jams & afternoon showcases on the newly christened Hanner Stage.
The main stage on this Canada Day afternoon was opened by the fantastic [Quw'utsun' T'zinquaw Dancers](https://www.youtube.com/watch?v=4Vnh7ICVGRA&ab_channel=AnneMarieThornton), who put on an amazing and poignant special performance that left the large crowd in the bowl awestruck and cheering.  who put on an amazing and poignant special performance that left the large crowd in the bowl awestruck and cheering. Also on the main stage were stand-out sets from [R.O Shapiro](https://roshapiro.com/) & [Dear Rouge](https://www.dearrouge.com/).  [Sugar Ray](https://www.sugarray.com/) put on a nostalgic, hilariously self-deprecating and high-energy set full of earworm hits, with plenty of nods to Canadian lifestyle and 90’s culture.  The [Wu-Tang Clan](https://wutangclan.com/) took the stage after the bleach-tipped pop rocker and delivered an incredible set to an exceptionally appreciative crowd, including a dedicated portion where the kids in the audience were invited up to the stage for a dance party of their own. Wu-Tang, after all, is for the children!

The party kept going with the always fabulous Goldfish closing out the main stage, and tour de force performances on the Lakenight stage by [La Chuparosa](https://www.facebook.com/LaChuparosaMusic/), [Kimmortal](https://kimmortalportal.com/), [Case Of The Mondays](https://www.facebook.com/caseofthemondaysofficial/), [DJ Anger](https://www.instagram.com/deejayanger/?hl=en), [K+Lab](https://soundcloud.com/klabnz), and [Murge](https://djmurge.com/), keeping the thousands in attendance dancing until late.

Saturday rounded the weekend out with spectacular midday main stage sets by [Old Soul Rebel](https://open.spotify.com/artist/6I3WzI96mccqCYFasXQYgB), [Daniel Wesley](https://danielwesley.com/), [Five Alarm Funk](http://www.fivealarmfunk.com/), & [JJ Wilde](https://jjwilde.com/).  Canadian rockers [The Glorious Sons](https://www.theglorioussons.com/) delivered a program that had the thousands packing the festival bowl erupting. A powerful stage-closing set by [The Halluci Nation](https://thehallucination.com/) was the perfect punctuation at the end of a flawlessly fantastic main stage weekend. Up at the Lakenight stage, the fun was kept alive with [Nostic & Nicki](https://www.nosticnicki.com/), [Missy D](https://www.missydmusic.com/), [Khanvict](https://www.khanvict.life/), [Illvis Freshly & Mt Doyle](https://soundcloud.com/illvisfreshly), [Astrocolor](https://www.astrocolormusic.com/), and with [Dunks](https://soundcloud.com/dunks) putting in the final pin of Laketown Shakedown 2022.

We couldn’t have asked for a better first weekend back to summer festivals with all of you. From the bottom of our hearts, THANK YOU!

Check out the recap video and photos below.


![](https://www.datocms-assets.com/64219/1661904244-laketown-shakedown-rms-media-46.jpg)

![](https://www.datocms-assets.com/64219/1661904252-laketown-shakedown-rms-media-44-1.jpg)

![](https://www.datocms-assets.com/64219/1661904257-johanna-vanderpol-laketown-jul-1-2022-copy-1.jpg)

![](https://www.datocms-assets.com/64219/1661904261-colin-smith-takes-pics-2021-136.jpg)

![](https://www.datocms-assets.com/64219/1661904266-colin-smith-takes-pics-2021-86-2.jpg)

![](https://www.datocms-assets.com/64219/1661904270-colin-smith-takes-pics-2021-35.jpg)

![](https://www.datocms-assets.com/64219/1661904275-colin-smith-takes-pics-2021-136-1.jpg)

![](https://www.datocms-assets.com/64219/1661904283-aturday-fun-rms-media-by-rob-porter-4.jpg)

![](https://www.datocms-assets.com/64219/1661904287-2v7a9006.jpg)

![](https://www.datocms-assets.com/64219/1661904291-2v7a8901.jpg)

![](https://www.datocms-assets.com/64219/1661904295-2v7a8880.jpg)

![](https://www.datocms-assets.com/64219/1661904300-20220702-zenon-kai-imaging-laketown-shakedown-saturday-dsc_7715.jpg)
![](https://www.datocms-assets.com/64219/1661904279-colin-smith-takes-pics-2021-100.jpg)

![](https://www.datocms-assets.com/64219/1655883426-img_6559-edit-2.jpg)

![](https://www.datocms-assets.com/64219/1661826246-demi-2022june30-26.jpg)

![](https://www.datocms-assets.com/64219/1661826253-laketown-shakedown-rms-media-47.jpg)

![](https://www.datocms-assets.com/64219/1661826262-dem_8267.jpg)

![](https://www.datocms-assets.com/64219/1661826299-colin-smith-takes-pics-2021-73-1.jpg)

![](https://www.datocms-assets.com/64219/1661826312-colin-smith-takes-pics-2021-29-1.jpg)

![](https://www.datocms-assets.com/64219/1661826319-colin-smith-takes-pics-2021-224.jpg)

![](https://www.datocms-assets.com/64219/1661826331-colin-smith-takes-pics-2021-107-1.jpg)

![](https://www.datocms-assets.com/64219/1661826337-colin-smith-takes-pics-2021-116-1.jpg)

![](https://www.datocms-assets.com/64219/1661826348-colin-smith-takes-pics-2021-223.jpg)

![](https://www.datocms-assets.com/64219/1661841134-colin-smith-takes-pics-2021-67-2.jpg)

![](https://www.datocms-assets.com/64219/1661841142-dem_6586.jpg)

![](https://www.datocms-assets.com/64219/1661841205-colin-smith-takes-pics-2021-13-2.jpg)

![](https://www.datocms-assets.com/64219/1661841254-colin-smith-takes-pics-2021-53-2.jpg)

**For more photos, please click** [**HERE**](https://www.rmsmedia.ca/Laketown-Shakedown-June-30-July-2-2022)

<iframe align="center" title="vimeo-player" src="https://player.vimeo.com/video/726908086?h=6154aab96e" width="640" height="360" frameborder="0" allowfullscreen></iframe>


# Laketown Rock
**July 22 - 23**

After a two-week hiatus, our awesome production teams were rigging the stages once again for our 2-day annual rock and roll showcase, Laketown Rock. Droves of rock fans made their way up to the Ranch for the Friday and Saturday affair. LTRock 2022 was full of festival goers of all ages enjoying the warm summer evenings in the festival bowl.

Friday featured Victoria rockers [The Poubelles](https://www.facebook.com/thepoubelles), as well as the locally acclaimed 4 piece [Liam Mackenzine & The Moondogs](https://open.spotify.com/album/74eh8wrqKaYpXatL9WjgZw) sharing the main stage with Canadian multi-Juno nominees [Wide Mouth Mason](https://widemouthmason.com/), and the man with no regrets, [Tom Cochrane](http://www.tomcochrane.com/).  Tom did not disappoint, singing his hits at sunset to a crowd also serenading him back. It was a magical first night to what would be one of our best editions of LTRock to date.

Once the main stage closed, local party band [Littlehead](http://wfsites-to.websitecreatorprotool.com/9168754b.com/home.html) put on a dance party to be remembered at the Flats stage.

Day two kicked off with a terrific showing by [Sarah Osborne & The Magic Buttons](https://www.sarahosbornemusic.com/live), followed by a stunner of a set by Campbell River’s up and coming old-soul blues rocker, [Garret T. Willie](https://www.youtube.com/playlist?list=UU-0Y3XNZyfi_u8Q69Wg1-Dw).  Canadian legends, and five-time Juno award winners [Big Sugar](https://www.bigsugar.com/) hit the stage, playing their familiar hits to an adoring crowd. The Junos were traded in for Grammy awards after that with Tennessee’s own blues icon [Keb' Mo'](https://kebmo.com/), who put on a master class of how contemporary blues is done in the American south.

Putting the cherry on the top of the festival sundae was none other than the fortunate son himself, [John Fogerty](https://johnfogerty.com/).  At 77 years old, he belted out everyone’s favourite CCR and solo works from over the past 50 years. He even played all the quintessential classics using the exact same guitar used at Creedence’s famed Woodstock performance. Keeping the feet tapping and singalongs going were the fantastic local cover band Pulse Radio, who delivered a perfect night cap after-party to a packed Flats stage.

Check out the recap video and photos below.

![](https://www.datocms-assets.com/64219/1661905291-tamara-mitchell-laketown-rock-july-23-2022-33-of-37.jpg)

![](https://www.datocms-assets.com/64219/1661905296-tamara-mitchell-laketown-rock-july-23-2022-29-of-37.jpg)

![](https://www.datocms-assets.com/64219/1661905301-tamara-mitchell-laketown-rock-july-23-2022-19-of-37.jpg)

![](https://www.datocms-assets.com/64219/1661905305-demi-2022july22-106.jpg)

![](https://www.datocms-assets.com/64219/1661905309-demi-2022july22-099.jpg)

![](https://www.datocms-assets.com/64219/1661905317-colin-smith-takes-pics-2021-54.jpg)

![](https://www.datocms-assets.com/64219/1661905313-colin-smith-takes-pics-2021-63.jpg)

![](https://www.datocms-assets.com/64219/1661905322-colin-smith-takes-pics-2021-5.jpg)

![](https://www.datocms-assets.com/64219/1661905326-colin-smith-takes-pics-2021-43.jpg)

![](https://www.datocms-assets.com/64219/1661905330-colin-smith-takes-pics-2021-38-1.jpg)

![](https://www.datocms-assets.com/64219/1661905334-colin-smith-takes-pics-2021-17.jpg)

![](https://www.datocms-assets.com/64219/1661905338-colin-smith-takes-pics-2021-14.jpg)

![](https://www.datocms-assets.com/64219/1661905343-colin-smith-takes-pics-2021-13.jpg)

![](https://www.datocms-assets.com/64219/1661840946-colin-smith-takes-pics-2021-98.jpg)

![](https://www.datocms-assets.com/64219/1661840895-colin-smith-takes-pics-2021-22.jpg)

![](https://www.datocms-assets.com/64219/1661841011-demi-2022july22-101-edit.jpg)

![](https://www.datocms-assets.com/64219/1661841017-laketown-rock-22-july-2022-afternoon-rms-media-by-rob-porter-29.jpg)

![](https://www.datocms-assets.com/64219/1661841000-demi-2022july22-061.jpg)

![](https://www.datocms-assets.com/64219/1661840993-demi-2022july22-076.jpg)

![](https://www.datocms-assets.com/64219/1661840980-demi-2022july23-14.jpg)

![](https://www.datocms-assets.com/64219/1661840971-colin-smith-takes-pics-2021-99.jpg)

![](https://www.datocms-assets.com/64219/1661840942-colin-smith-takes-pics-2021-91-1.jpg)

![](https://www.datocms-assets.com/64219/1661840937-colin-smith-takes-pics-2021-98-1.jpg)

![](https://www.datocms-assets.com/64219/1661840920-colin-smith-takes-pics-2021-78.jpg)

![](https://www.datocms-assets.com/64219/1661840914-colin-smith-takes-pics-2021-47.jpg)

![](https://www.datocms-assets.com/64219/1661840905-colin-smith-takes-pics-2021-36-1.jpg)

![](https://www.datocms-assets.com/64219/1661840901-colin-smith-takes-pics-2021-104-1.jpg)

**For more photos, please click** [**HERE**](https://www.rmsmedia.ca/Laketown-Rock-July-22-23-2022)

<iframe title="vimeo-player" src="https://player.vimeo.com/video/733373245?h=5496aa2124" width="640" height="360" frameborder="0" allowfullscreen></iframe>


# Sunfest Country Music Festival
**July 28 - 31**

Less than a week after Laketown Rock, we reopened the barn doors that have been closed for far too long. Sunfest 2022 kicked off its 4 day program to the thousands who make the yearly pilgrimage to Laketown Ranch for the island’s largest music festival.

The weather showed up too, with the mercury tipping close to 40º on Friday. Despite the hottest days of summer, attendees were largely unfazed, basking in cloudless days, and partying well into nights that didn’t drop below 23º before the sun came up.

The 2022 program featured a dizzying array of world-class performances, by world-class performers on world-class stages. Thursday had outstanding main stage performances by Sunfest alum [Dallas Smith](https://dallassmithmusic.com/), Tennessee's own [Rodney Atkins](https://www.rodneyatkins.com/), Canada's 'Country Music Gentleman' [Ben Klick](https://www.benklick.com/), along with the island's own [Ryan McMahon](https://ryanmcmahon.com/), as well as Vancouver Country & Folk group [The Promised](https://thepromisedmusic.com/).

Friday’s program included The Guess Who & BTO’s incomparable songwriter [Randy Bachman](https://www.randybachman.com/), massive rising star [Orville Peck](https://www.orvillepeck.com/), & Vancouver's favourite country daughter [Madeline Merlo](https://www.madelinemerlo.com/) gracing the main stage, among others.  That evening, [Aaron Pritchett](https://open.spotify.com/artist/5cX6PE94aP77FQP0YVHNPX) performed to a massive crowd at the Lakenight stage, with [Mt Doyle](https://soundcloud.com/mt-doyle-eats-pizza) taking them on a journey afterwards.

On Saturday, the big stage hosted incredible sets by [Mackenzie Porter](https://mackenzieporter.com/), [Kip Moore](https://www.kipmoore.net/), and capped off with an amazing offering by Hootie & The Blowfish frontman, [Darius Rucker](https://www.dariusrucker.com/), singing his solo hits and favourites from the aforementioned iconic 90’s South Carolina pop rock band. Lakenight stage stand out sets from [Mbira Spirit](http://www.mbiraspirit.com/), [Born Reckless](https://www.bornrecklessband.com/) & the [James Barker Band](https://www.jamesbarkerband.com/) ran the evening out with [DJ Mike Devlin](https://www.timescolonist.com/writers/mdevlin) taking the reins for the dance party.

The heat dipped slightly on Sunday, and provided a comfortable climate for dancing along to performances from artists [High Quadra Ramblers](https://www.highquadraramblers.com/), [Dave Hartney](https://www.davehartney.com/), [Shred Kelly](http://www.shredkelly.com/), followed by back to back Mississippi country stars, [Randy Houser](https://www.randyhouser.com/) & [Hardy](https://hardyofficial.com/).  [Chris Andres](https://officialchrisandres.com/), [Michael Daniels](https://www.officialmichaeldaniels.com/), & the [Hunter Brothers](https://hunterbrothers.com/) all gave stellar offerings to a full Lakenight stage all evening, with JRFM's own [Jaxon Hawks](https://soundcloud.com/jaxon-hawks) bringing the weekend home.

It truly was a Sunfest that ranks among the very best, and we owe it all to those who made it possible after a long 3-year break; all of our awesome vendors and sponsors, indispensable ranch hands & staff, incredible stage, FOH and production crew, and of course YOU! Thank you!

**See y'all next summer!**

Check out the recap video and photos below.

![](https://www.datocms-assets.com/64219/1661905917-tfalkchalmers-9393.jpg)

![](https://www.datocms-assets.com/64219/1661905923-tfalkchalmers-81.jpg)

![](https://www.datocms-assets.com/64219/1661905927-tfalkchalmers-73.jpg)

![](https://www.datocms-assets.com/64219/1661905931-tfalkchalmers-6148.jpg)

![](https://www.datocms-assets.com/64219/1661905935-tamara-mitchell-sunfest-july-30-2022-28-of-69.jpg)

![](https://www.datocms-assets.com/64219/1661905940-love-like-this-dsc_5078.jpg)

![](https://www.datocms-assets.com/64219/1661905946-love-like-this-dsc07237-1.jpg)

![](https://www.datocms-assets.com/64219/1661905949-love-like-this-dsc06913.jpg)

![](https://www.datocms-assets.com/64219/1661905953-love-like-this-dsc06894.jpg)

![](https://www.datocms-assets.com/64219/1661905957-love-like-this-dsc06867.jpg)

![](https://www.datocms-assets.com/64219/1661905962-love-like-this-dsc06838.jpg)

![](https://www.datocms-assets.com/64219/1661905966-img_8743.jpg)

![](https://www.datocms-assets.com/64219/1661905969-demi-2022july28-115.jpg)

![](https://www.datocms-assets.com/64219/1661852627-tfalkchalmers-328.jpg)

![](https://www.datocms-assets.com/64219/1661852672-sunfest-29-july-2022-rms-media-by-rob-porter-22.jpg)

![](https://www.datocms-assets.com/64219/1661852837-tfalkchalmers-493.jpg)

![](https://www.datocms-assets.com/64219/1661852807-tfalkchalmers-3910.jpg)

![](https://www.datocms-assets.com/64219/1661852789-tfalkchalmers-338.jpg)

![](https://www.datocms-assets.com/64219/1661852735-tfalkchalmers-16.jpg)

![](https://www.datocms-assets.com/64219/1661852725-tfalkchalmers-330.jpg)

![](https://www.datocms-assets.com/64219/1661852710-love-like-this-dsc_3167.jpg)

![](https://www.datocms-assets.com/64219/1661852861-tfalkchalmers-535.jpg)

![](https://www.datocms-assets.com/64219/1661852876-tfalkchalmers-201.jpg)

![](https://www.datocms-assets.com/64219/1661852892-tfalkchalmers-20.jpg)

![](https://www.datocms-assets.com/64219/1661852899-demi-2022july28-125.jpg)

![](https://www.datocms-assets.com/64219/1661852904-demi-2022july29-31.jpg)

![](https://www.datocms-assets.com/64219/1661852942-love-like-this-dsc07496.jpg)

**For more photos, please click** [**HERE**](https://www.rmsmedia.ca/Sunfest-July-28-31-2022)

<iframe title="vimeo-player" src="https://player.vimeo.com/video/735645771?h=3b10f02fdf" width="640" height="360" frameborder="0" allowfullscreen></iframe>