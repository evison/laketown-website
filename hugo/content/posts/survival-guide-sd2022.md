---
title: Survival Guide - Laketown Shakedown | Laketown Ranch
type: post
layout: post
card_image: 'https://www.datocms-assets.com/64219/1653978822-sunday-laketown-shakedown-colin-smith-takes-pics-2019-80.jpg?w=1260&h=630&q=50&fit=crop&fm=webp'
hero_image: 'https://www.datocms-assets.com/64219/1653978822-sunday-laketown-shakedown-colin-smith-takes-pics-2019-80.jpg?w=1920&h=1080&q=60&fit=crop&fm=webp'
hero_heading: Survival Guide - Laketown Shakedown
excerpt: Features & Key Things To Remember
slug: survival-guide-sd2022
card_tag: Laketown Shakedown 2022
date: 2022-06-28T00:00:00.000Z
share_title: Key info for Laketown Shakedown
share_url: 'https://laketownranch.com/posts/survival-guide-sd2022'
share_description: Find your way around the Ranch
share_image: 'https://www.datocms-assets.com/64219/1653978822-sunday-laketown-shakedown-colin-smith-takes-pics-2019-80.jpg?w=1200&h=630&q=60&fit=crop&fm=webp'
---

We are just **two days** away from the return of our summer festival programming at Laketown Ranch!

After a long hiatus, we are extrememly excited to welcome you all back to Laketown Shakedown on this Thursday!

![](https://www.datocms-assets.com/64219/1656392935-laketownshakedown_fb_1920x1080_fivealarmfunk_02.jpg)

It's been a while, so to better prepare you for your weekend of fun and frolic at the Ranch, we've put together some reminders to help you find your way around.
Let's squeeze some mind grapes!

# Lower Camping Area & Flats
The lower camping area is typically where you'll find your friends RV.  It's expansive, and organized into different sections & street names.  Looking up Main Street from the parking area, you have your streets on the left of Main, and your roads to the right.  They're all named after musicians, to better help you remember where you and your gang have all set up camp.

![](https://www.datocms-assets.com/64219/1656393120-ltsd-sitemap-2022-lower_camp-2400.jpg)

Up at the top of Lower Camping, is the Laketown Flats.  The Flats stage is playing host to some amazing local talent during matinee shows on Friday & Saturday.  Be sure to check the schedule! You'll also just want to head to this area in general for food, drink, washrooms, showers, to use as an easy meetup point, or just for some general respite.  Have a question?  Need a hat?  Hungry?  Out of cash?  Get a sliver?  First aid, merchadise, food vendors, ATMs, festival information, as well as the RCMP office are all found in The Flats area.  Showers and flush toilets can also be found in tis zone.

# Festival Bowl & Main Stage
A lot of action going on in the bowl!  Hungry?  Thirsty?  Gotta pee?  You'll find food vendors, a bar, water, accesible washrooms, and smoking sections along the west side of the bowl.  Is your favourite artist playing and you want to get closer to it all?  VIP and Stage Pit ticket upgrades are available in the upper centre of the bowl.  Also a good place to grab some merch to wear once you get up to the front!  Merchardise booths are found in this area as well, along with ATMs.  Want to take a break and watch from above?  Head on over to the Hillside on the east side of the bowl to take in the sights and sounds from the knoll.  The Family Zone is also on the Hillside.  So is the zipline!

![](https://www.datocms-assets.com/64219/1653975278-sunday-laketown-shakedown-colin-smith-takes-pics-2019-124.jpg)

![](https://www.datocms-assets.com/64219/1655799549-ltsd-sitemap-2022-festival_bowl-2400.jpg)

# Lakenight Stage
While you're on the Hillside, note the Lakenight Stage at the tippy top of the bowl area.  You'll be coming back here later.  Sooner than later in fact.  Check the schedule.  Set some alarms, and watch your watch.  There's food, water, washrooms, all the amenities you need up here too.

![](https://www.datocms-assets.com/64219/1653979275-5d4_3366-edit.jpg)

# Upper Camping & Hanner Stage
You staying up in the top of the property?  Camping probably?  Then you need to know your way around!  Dissimilarly to the U2 song, here the streets do in fact have names.  Remember to remember which streetname you're on, and check out the maps to find your way home!  You'll find the Hanner stage at the beginning of your walk home up here too, so stop in for a dance and make some friends on your way.  The Hilltop Food Stop is also up here for you to grab some grub.

![](https://www.datocms-assets.com/64219/1655855653-annemariesphotography_sunfestcountry2019_sunday-c-bcphotographer-371-sm.jpg)

Also up at the top of the property are a web of different hiking and biking trail routes.  The vistas at the top of the plateau are amazing, so why not go for a little walk during the day?  You'll be happy to you did!

# Shuttles
There is shuttle service available to and from Duncan, as well as Lake Cowichan. Note the schedule below:

Find the shuttles at **GATE 1**

**Thursday/Saturday Shuttle Schedule - Duncan to Laketown Ranch**

Departs Island Savings Centre Duncan starting at 2:00 pm and then every 30 minutes until 1:00 am
Departs Laketown Ranch starting at 2:45 pm and then every 30 minutes until 2:15 am
Shuttle Cost from Duncan to Laketown Ranch is $10 each way, or $15 round trip, and is payable at Laketown Ranch to our shuttle greeters.

**Private Shuttles and Hotel PickUps:**

Cheers Cowichan cheerscowichan@gmail.com
Trips for Tips - 250-210-0518
Duncan Taxi 250-746-4444

The local shuttles are running on a continuous loop from Laketown Ranch to Saywell Park in Lake Cowichan. Operation times are Thursday, 2:00pm to 2:00am and Friday/Saturday 11:00am - 2:00am. The fee is $5 per trip and is payable upon boarding.  *Note that this shuttle offers accesibility options.

**Please note that traffic/road conditions may affect shuttle schedule.

# Quick But Important Notes
* DRINK WATER!
* WEAR SUNGLASSES!
* SUNSCREEN TOO!
* USE THE BUDDY SYSTEM!
* KIDS LOVE EARPLUGS!
* NO READMISSION TO THE CONCERT BOWL AFTER 8PM!
* KNOW YOUR LIMITS, STAY WITHIN THEM!
* DON'T LITTER!
* STAY OFF THE **SIN WAGON**!
* WATCH YOUR FOOTING!
* BE NICE!
* MAKE NEW FRIENDS!
* HAVE FUN!
* PROTECT YA NECK!

The property is home to all sorts of wildlife.  We've seen herds of elk each night this week, and a bear, which is amazing, but it's also a good reminder that we are guests here, and we all need to respect that this is their home.  Pick up your trash, don't put ciggy butts on the ground, and do not approach the wildlife.. Even if you're almost positive that it's just someone dressed in a furry wolf costume.


**Early Bird Camping**
Early bird load in starts at 12pm on Wednesday and runs from 10am - 12pm on Thursday.
Show up on time to your slot!  This doesn't mean early!  It's important to not come before your selected entry time, as you won't be granted access until your load in time, and parking on the highway is not permitted!

# Schedule & App
Plan your day accordingly so you don't miss your favourite artists!  Keep eyes on the schedule!  There are Info Hubs sprinkled around the site that include the schedule, maps, and key info.  There's also a QR code to download the Laketown Ranch App, allowing you to keep all the info in your pocket.  The QR code looks like this:

![](https://www.datocms-assets.com/64219/1656450400-laketown_ranch_app_page-300.jpg)

![](https://www.datocms-assets.com/64219/1656392594-sd2022-schedule-1200-2-web.jpg)