---
title: Laketown Rock Highlights & Survival Guide | Laketown Ranch
type: post
layout: post
card_image: 'https://www.datocms-assets.com/64219/1658309498-laketownrock-2022-poster-1920-v2-with-pulse-radio-flattened.jpg?w=1260&h=630&q=50&fit=crop&fm=webp'
hero_image: 'https://www.datocms-assets.com/64219/1658309498-laketownrock-2022-poster-1920-v2-with-pulse-radio-flattened.jpg?w=1920&h=1080&q=60&fit=crop&fm=webp'
hero_heading: Laketown Rock Highlights & Survival Guide
excerpt: Laketown Rock Highlights & Survival Guide
slug: laketownrock2022-highlights-survival-guide
card_tag: Laketown Rock 2022
date: 2022-07-18T00:00:00.000Z
share_title: Laketown Rock Highlights & Survival Guide
share_url: 'https://laketownranch.com/posts/laketownrock2022-highlights-survival-guide'
share_description: What to Expect at Laketown Rock 2022
share_image: 'https://www.datocms-assets.com/64219/1658309498-laketownrock-2022-poster-1920-v2-with-pulse-radio-flattened.jpg?w=1200&h=630&q=60&fit=crop&fm=webp'
---

Ranchin' During Laketown Rock...

# MAIN STAGE

![](https://www.datocms-assets.com/64219/1658216334-tamara-mitchell-laketown-rock-207-of-428-2.jpg)

**Friday, July 22**

After what seems like years (because it was), Laketown Rock is BACK!
**Gates open at 4pm**. 

Starting us off for the weekend at 4:45pm with their infectious, hook-driven sound are Victorian pop rockers, [The Poubelles](https://open.spotify.com/artist/0EJRIHQIutHUIlq9CXqzIp), followed by high-energy, heartland rockers, [Liam Mackenzie & The Moondogs](https://open.spotify.com/album/74eh8wrqKaYpXatL9WjgZw)!

![](https://www.datocms-assets.com/64219/1647195636-ltrock_2022-wide_mouth_mason-lineup.jpg)

Saskatchewan's own multi-time Juno Nominees [Wide Mouth Mason](https://open.spotify.com/artist/6CcCAJi97tqh5OFAwy1THH) kick off at 7:15pm, warming up the stage at 9:00 for Mr. Big League himself, the man with No Regrets, [Tom Cochrane](https://open.spotify.com/artist/5Jj4mqGYiplyowPLKkJLHt)!

![](https://www.datocms-assets.com/64219/1647195596-ltrock_2022-tom_cochrane-lineup.jpg)

**Saturday, July 23**
Get ready for an amazing day two on the Main Stage!
**Gates open at 3pm**.

We're happy to have singer, songwriter and story teller [Sarah Osborne & the Magic Buttons](https://open.spotify.com/artist/5erYlGGHyZ7RH3sctmS74f) take the stage to start things off at 4pm, leading up to what will be a not-to-miss performance by the Island's own blues protege, [Garret T Willie](https://www.youtube.com/playlist?list=UU-0Y3XNZyfi_u8Q69Wg1-Dw)!

![](https://www.datocms-assets.com/64219/1647195400-ltrock_2022-big_sugar-lineup.jpg)

Torontonian alt-rockers [Big Sugar](https://open.spotify.com/artist/75eraSeadYDXU4zyzDxglZ) hit the stage at 5:45pm, playing what will be a favourite filled sing-along set!  After that, we have the five-time Grammy Award winner, blues singer/songwriting legend, [Keb Mo](https://open.spotify.com/artist/6iDaoPZVgxrTkndDCisX8F?autoplay=true)!

![](https://www.datocms-assets.com/64219/1647195336-ltrock_2022-keb_mo-lineup.jpg)

At 9pm, for his first ever concert visit to the Cowichan Valley, The Forntuate Son, [John Fogerty](https://open.spotify.com/artist/5ujCegv1BRbEPTCwQqFk6t?autoplay=true) graces the Laketown Ranch stage.

![](https://www.datocms-assets.com/64219/1647195260-ltrock_2022-john_fogerty-lineup.jpg)

# FLATS STAGE

![](https://www.datocms-assets.com/64219/1658216397-tamara-mitchell-laketown-rock-428-of-428-2.jpg)

Capping things off each night will be performances on the Laketown Flats Stage.
On **Friday** at 10:30pm we have Vancouver Island rockers, [Littlehead](https://open.spotify.com/artist/0qQNhpRFsrOmeaovM7OP2S?autoplay=true)!

**Saturday** night at 10:30pm, jumping-dance-floor enthusiasts, [Pulse Radio](https://www.facebook.com/PulseRadioBand) will be playing all your favourite songs to cap the weekend off.

Phew!  Gonna be a fun one.  See you all up there!

# KNOW YOUR WAY AROUND
The lower camping area is typically where you’ll find your friends RV. It’s expansive, and organized into different sections & street names. Looking up Main Street from the parking area, you have your streets on the left of Main, and your roads to the right. They’re all named after musicians, to better help you remember where you and your gang have all set up camp.

![](https://www.datocms-assets.com/64219/1652554584-ltrock-sitemap-2022-lower_camp-2400pxw-web-20220513.jpg)

Up at the top of Lower Camping, is the Laketown Flats. The Flats stage is playing host to some a couple amazing local bands after the main stage closes on both Friday & Saturday (see above). You’ll also just want to head to this area in general for food, drink, washrooms, showers, to use as an easy meetup point, or just for some general respite. Have a question? Need a hat? Hungry? Out of cash? Get a sliver? Festival Health Services, merchadise, food vendors, ATMs, festival information, as well as the RCMP office are all found in The Flats area. Showers and flush toilets can also be found in this zone.

# FESTIVAL BOWL
A lot of action going on in the bowl! Hungry? Thirsty? Gotta pee? You’ll find food vendors, a bar, water, accesible washrooms, and smoking sections along the west side of the bowl. Is your favourite artist playing and you want to get closer to it all? VIP and Stage Pit ticket upgrades are available in the upper centre of the bowl. Also a good place to grab some merch to wear once you get up to the front! Merchardise booths are found in this area as well, along with ATMs. Want to take a break and watch from above? Head on over to the Hillside on the east side of the bowl to take in the sights and sounds from the knoll. The Family Zone is also on the Hillside.

![](https://www.datocms-assets.com/64219/1658377449-ltrock2022-map-festivalbowl-2400.jpg)

# SHUTTLES
There are shuttle service for both local Lake Cowichan trips, and Duncan trips.
Check out the schedules below!
![](https://www.datocms-assets.com/64219/1658377083-laketownrock_shuttles_1080x1080_duncan-laketownranch_04.jpg)
![](https://www.datocms-assets.com/64219/1658377095-laketownrock_shuttles_1080x1080_lakecowichanlocal_04.jpg)
![](https://www.datocms-assets.com/64219/1658377101-laketownrock_shuttles_1080x1080_additionalservices_04.jpg)
