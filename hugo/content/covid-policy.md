---
title: Covid 19 Policies
type: page
layout: page-no-hero
hero_heading: Covid 19 Policies
app_promo: true
---

## LAKETOWN RANCH COVID-19 VACCINATION POLICIES

Our top priority is to provide a safe Laketown Ranch experience for guests, artists, staff, volunteers, and community members. Our policies and safety plans are always informed by the latest information from the BC Provincial Health Officer (PHO), the BCCDC and local health authorities.

As outlined in the PHO’s March 10th, 2022 announcement, as of April 8th, 2022 proof of receiving both COVID-19 Vaccinations is no longer required. As a result of this we have changed our policy. Vaccine passports will no longer be required to attend events at Laketown Ranch.

If we receive further direction from the PHO or update our internal policies, any new information about the health and safety requirements will be provided and communicated to you via the email address you used to purchase or receive your event tickets.

Thank you and see you all this summer.
