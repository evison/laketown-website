---
title: Camping | Laketown Ranch
type: page
layout: page-no-hero
hero_heading: COVID-19 SAFETY ADVISORY
---

<div><img src="/static/images/laketown-ranch-campgrounds.svg" alt="Laketown Ranch Campgrounds" style="display:block;margin:auto;" /></div>

<h1 class="a-heading--underlined">COVID-19 Safety Advisory</h1>

Thank you for taking a moment to read our COVID-19 safety notice.

To limit the spread of COVID-19, the Provincial Health Officer has issued orders that impact the camping industry. These orders outline conditions and provide specific direction regarding the services provided at Laketown Ranch Campgrounds. As a result we have implemented safety measures that will be revised as needed based on provincial and district direction. These measures apply to all staff and guests.

We will be following all government directives, and we will be utilizing best practices and recommendations. We will strictly follow social distancing standards and expect campers to do the same.

All staff and guests are asked to review the following questions before arriving:

_Do any of the members of your travelling party have the following symptoms of COVID-19: fever, cough, difficulty breathing, diminished taste or smell, runny nose, sore throat, muscle or body aches?_

_Have any of your party had contact with a person with a confirmed or possible case of COVID-19 in the past 21 days?_

_Has anyone in your party travelled outside of Canada in the past 14 days, or had contact with anyone that has just returned to Canada from abroad?_

If you respond **YES** to any of the points above, we ask that you postpone travelling until the following conditions are met or until you have been tested for COVID-19 and have received a negative result.

If you responded **NO** to the points above it is still very important that you remain aware of special measures at Laketown Campgrounds, and comply with recent changes to government regulations and guidelines regarding COVID-19.

PHO guidelines for gatherings may change between the time of booking and a scheduled arrival date. Group camping parties MUST meet current PHO guidelines for gatherings, even if those numbers are less than the maximum site allowance. Ranch staff will be monitoring for compliance.

It is imperative that our guests behave in a socially responsible manner when it comes to the situation surrounding the Covid-19 pandemic. At all times:

_You must maintain a physical distance of two metres from others._

_Practice diligent hand hygiene at all times by washing with plain soap and water for at least 20 seconds or by using alcohol-based hand sanitizer._

_Practice cough etiquette by coughing into your elbow or covering your mouth and nose with a disposable tissue when you sneeze. Immediately dispose of used tissues and wash your hands._

_Do not touch your eyes, nose or mouth with unwashed hands. Do not share food, drinks, utensils, cigarettes, vaping devices, joints or bongs._

**Any guests who fail to follow our directives and procedures in regards to Covid-19 will be asked to leave the property.**

Enhanced cleaning practices will include the routine cleaning of frequently touched surfaces, increased usage of disinfectants, placement of hand sanitizer dispensers throughout the grounds, and employee usage of personal protection equipment.

COVID-19 safety minded operations also include social distance check-ins. No early check ins and late check outs are not permitted during this time, to allow our staff time to safely clean before arrivals.

During Covid any new reservations that are created are subject to our standard cancellation policy, unless otherwise specified by Ranch staff. Our cancellation policy and all other policies can be found on our [Policies Page](/campground-policies).

We thank you again for taking the time to read our notice and we look forward to welcoming you at Laketown Ranch this season. If you have any questions please email us at [campground@laketownevents.com](mailto:campground@laketownevents.com).
