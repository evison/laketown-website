---
title: Home | Laketown Ranch
type: page
layout: index
is_home: true
heading: "Western Canada's Premier Festival & Recreation Park"
contact_form: true
contact_heading: Venue Inquiry
contact_body: Want to host your event at Laketown Ranch or have another question?
events: { id: "events", heading: "What's On At Laketown", body: "" }
news_heading: "Laketown News"
hero_image: /static/images/hero-poster-mobile.jpg
acknowledgement: "We acknowledge that we are on the unceded traditional territory of Ts’uuubaa-asatx, the First Peoples of the Cowichan Lake area. We thank Ts’uubaa-asatx for allowing us to continue to live, operate, and recreate on these beautiful lands."
---
