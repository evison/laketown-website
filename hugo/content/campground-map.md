---
title: Summer Camping Map
type: page
layout: page-no-hero
hero_image: ''
hero_heading: Summer Camping Map
hero_map: null
contact_form: null
contact_heading: Contact Form
contact_body: ''
faq:
  questions: []
---

<div><img src="/static/images/laketown-ranch-campgrounds.svg" alt="Laketown Ranch Campgrounds" style="display:block;margin:auto;" /></div>

# Summer Camping Map

<p class="a-h4">This map is for <strong>Summer Camping only</strong>. NOT FOR EVENT CAMPING. Event camping is booked with event pass purchase.</p>

<p class="a-h4">For full event maps, <a href="/event-maps">please click here</a></p>

<a href="#" data-featherlight="/static/images/campground-map-v1.jpg">
<img src="/static/images/campground-map-v1.jpg" alt />
</a>

<a download="Laketown-Ranch-Camground-Map.jpg" href="/static/images/campground-map-v1.jpg" title="Laketown Ranch Campground Map"><h4>DOWNLOAD MAP</h4></a>