---
title: Festival Camping
type: page
layout: info-faq
hero_image: 'https://www.datocms-assets.com/64219/1699731402-hero-camping.jpg'
hero_heading: Festival Camping
hero_map: false
contact_form: true
contact_heading: Contact Form
contact_body: Need more information?
faq:
  questions:
    - question: What do I need to enter the campgrounds?
      answer: 'To have access to the campgrounds you must have the relevant single day or weekend festival pass AND a camping wristband. Laketown Ranch is a festival venue that features camping, it is not a camping only event.'
    - question: I have rented an RV or trailer. Can the rental company drop it off?
      answer: 'Only if you absolutely cannot arrive with the vehicle. Your credentials including the RV pass must be sent with the driver. Once parked in your site, the driver can exit and return your credentials to you. You must have your credentials (except for the RV sticker) to enter.'
    - question: 'Once my site is set up, can I leave with my vehicle?'
      answer: 'Yes. Please do not leave during load-in times, as it causes congestion.'
    - question: My friends booked several campsites. We arrive at different times. Can we camp together?
      answer: 'If you are camping in a group, meet off-site and arrive together. Every effort will be made to place you in your preferred campsite, however, we cannot guarantee spots.'
    - question: My friends are camping with me and will arrive later. How can they find me?
      answer: 'Once your friends have arrived, they will be directed to your campsite. Plan ahead - if your friends are dropped or arrive by shuttle, don’t make them walk-in their gear.'
    - question: I’m being dropped off. Where do I go?
      answer: The drop-off area is at the entrance to the grounds. No drop-offs in the campground.
    - question: Where do I put full garbage and recycling bags?
      answer: There are garbage and recycling stations throughout the campgrounds. Please take your waste to pick-up locations next to the porta-potties.
    - question: Are campfires allowed?
      answer: 'Propane fire pits are allowed. Please keep propane fire pits 12 in (30 cm) off the ground. Have a fire extinguisher on site at your camp. NO WOOD FIRES! No wood fire pits, charcoal cooking devices or tiki torches are permitted on site. **Subject to provincial or local fire restrictions.'
    - question: Do campsite have power and water hook-ups?
      answer: No hook-ups of any kind are currently available. There are no RV water refill stations on-site. Fill before arrival.
---

{{< h2-underlined Info >}}

#### Camping
To have access to the campgrounds you must have the relevant single day or weekend festival wristband AND a camping wristband. Laketown Shakedown is a music festival that features camping, it is not a camping only event.

Cook stoves, propane fire pits and BBQ’s MUST be 12 in (30 cm) off the ground at all times. ABSOLUTELY NO WOOD FIRE PITS OR CHARCOAL COOKING DEVICES. You must have a fire extinguisher at your campsite if you are using a cook stove, BBQ or propane fire pit.

No pets allowed on site, with the exception of registered service dogs. If you have a service dog you must provide to Guest Services the BC Guide & Service dog registration.

Failure to keep a clean campsite WILL result in eviction from grounds. No household furniture permitted.

Climbing on any structures, trees, RV’s (with the exception of repairs), fences, etc., may lead to eviction from grounds.

Generators must be shut down from 12am to 8am. Obey campsite quiet times. Campsites must be left clean and vacated by 11am the day after the event.

Campers who do not demonstrate care for self, others or the environment WILL be told to leave the campground and will have their camping passes revoked without a refund. Camping at Laketown Ranch is a privilege. Dangerous or obnoxious behaviour will NOT be tolerated.

#### Transportation & Roadways
The roadway speed limit is 10 km/hr. Excessive speed causes dust and is dangerous. Those who drive over the speed limit will be subject to ejection. No cruising or excessive trips throughout the site are permitted.

No personal ATV’s, dirt bikes, bicycles or golf carts are permitted.

Keep roadways clear at all times. Vehicles impeding roadways will be towed.

Always arrange a safe ride home. Taxis are available on site and we have a comprehensive shuttle service. Road checks will be done nightly by local law enforcement.

#### Recycling & Garbage
LEAVE NO TRACE! Laketown Ranch is a pristine, natural environment that is home to herds of elk and other wildlife. Please help us keep it clean by utilizing the proper waste and recycling bins throughout the festival site. Garbage drop-off locations are located throughout camping. There is no curbside pickup. Please be responsible and use the bins provided.

Clean campsites will be rewarded. Dirty ones may be ejected at any time. Please treat Laketown Ranch as your own home and respect it.

Again, LEAVE NO TRACE!

#### Grounds For Ejection

* Invalid, tampered with, fraudulent or absent wristband (civil and/or criminal charges will be sought)
* Rowdy or disrespectful behaviour
* Excessive noise/noise after curfew
* Excessive garbage in campsite
* Sales of any unauthorized or illegal materials
* Possession of fireworks, weapons or illegal drugs
* Contributing to underage drinking
* Speeding on roadways
* Public intoxication

Be respectful of yourself, other guests, staff, volunteers, vendors, security, RCMP, and the beautiful natural environment around you.

{{< disclaimer >}}