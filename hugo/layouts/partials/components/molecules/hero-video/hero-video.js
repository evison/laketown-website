import $ from 'jquery';
import * as utilities from 'global/scripts/utils/utilities';

/**
 *
 */
class HeroVideo {

  /**
   *
   */
  init() {
    this.$component = $('.js-hero-video');
    if (this.$component.length) {
      this.$header = $('.js-header');
      this.$video = this.$component.find('.js-video');
      this.$gif = this.$component.find('.js-gif');
      this.$cta = this.$component.find('.js-cta');

      this.scrollTimeout = 0;
      this.isScrolling = false;
      if (!$('body').hasClass('no-touchevents')) {
        $(window).on('scroll', this.onScroll.bind(this));
      }

      this.$cta.on('click', this.onCtaClick.bind(this));
      $(window).on('resize', this.onResize.bind(this));
      $(window).on('load', this.onResize.bind(this));
      this.onResize();

      setTimeout(() => {
        this.onResize();
      }, 2000);
      setTimeout(() => {
        this.onResize();
      }, 5000);

      this.checkAutoplay();
    }
  }

  onScroll() {
    this.isScrolling = true;

    // Clear our timeout throughout the scroll
    window.clearTimeout(this.scrollTimeout);

    // Set a timeout to run after scrolling ends
    this.scrollTimeout = setTimeout(() => {
      this.isScrolling = false;
    }, 66);
  }

  checkAutoplay() {
    if (utilities.autoplaySupported()) {
      this.$gif.remove();

      const img = new Image();
      img.src = this.$video[0].getAttribute('poster');
      img.onload = this.onResize.bind(this);
    } else {
      this.$gif.css('background-image', this.$gif.data('bg'));
      this.$video.remove();

      let bg = this.$gif.css('background-image');
      bg = bg.replace('url(','').replace(')','').replace(/\"/gi, "");
      const img = new Image();
      img.src = bg;
      img.onload = this.onResize.bind(this);
    }
  }

  onCtaClick() {
    $('html, body').animate({
      scrollTop: window.innerHeight
    }, 1000, 'easeInOutExpo');
  }

  onResize() {
    if (utilities.currentBreakpoint().index > 2) {
      this.$video = this.$component.find('.js-video-desktop');
    } else {
      this.$video = this.$component.find('.js-video-mobile');
    }
    this.$video.attr('poster', this.$video.data('poster'));
    this.$video[0].play();

    this.$video.css('position', 'absolute');

    const margin = parseInt(this.$component.css('margin-left'));
    const headerHeight = this.$header.height();

    const screenWidth = window.innerWidth - 2 * margin;
    const screenHeight = window.innerHeight - (headerHeight + margin);
    const screenAspect = screenWidth / screenHeight;

    const videoAspect = this.$video.width() / this.$video.height();

    if (screenAspect < videoAspect) {
      this.$video.css('height', '');
      this.$video.width(screenHeight * videoAspect);
    } else {
      this.$video.css('width', '');
      this.$video.height(screenWidth / videoAspect);
    }

    if (!this.isScrolling) {
      this.$video.css({
        'margin-left': (screenWidth - this.$video.width()) / 2,
        'margin-top': (screenHeight - this.$video.height()) / 2
      });
      this.$component.css({
        width: screenWidth,
        height: screenHeight
      });

      this.$gif.height(screenHeight);
    }

    this.$component.css('opacity', 1);
  }
}

const heroVideo = new HeroVideo();
heroVideo.init();
