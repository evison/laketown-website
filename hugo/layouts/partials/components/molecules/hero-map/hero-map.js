import $ from 'jquery';
import * as utilities from 'global/scripts/utils/utilities';

/**
 *
 */
class HeroMap {

  /**
   *
   */
  init() {
    this.$hero = $('.js-hero-map');
    if (this.$hero.length) {
      this.$header = $('.js-header');
      $(window).on('resize', this.onResize.bind(this));
      this.onResize();

      this.scrollTimeout = 0;
      this.isScrolling = false;
      if (!$('body').hasClass('no-touchevents')) {
        $(window).on('scroll', this.onScroll.bind(this));
      }
    }
  }

  onScroll() {
    this.isScrolling = true;

    // Clear our timeout throughout the scroll
    window.clearTimeout(this.scrollTimeout);

    // Set a timeout to run after scrolling ends
    this.scrollTimeout = setTimeout(() => {
      this.isScrolling = false;
    }, 66);
  }

  onResize() {
    if (!this.isScrolling) {
      const headerHeight = this.$header.height();
      this.$hero.height(window.innerHeight - headerHeight);
      this.$hero.width(window.innerWidth);
      this.$hero.css('opacity', 1);
    }
  }
}

const heroMap = new HeroMap();
heroMap.init();