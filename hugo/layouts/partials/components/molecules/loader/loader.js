import $ from "jquery";

/**
 *
 */
class Loader {
  /**
   *
   */
  init() {
    this.$loader = $(".js-loader");
    this.$mainContainer = $(".js-main-container");
    this.$video = $(".js-video");
    this.$body = $("body");

    if (this.$loader.length && !sessionStorage.loader) {
      sessionStorage.loader = true;

      if (location.pathname === "/") {
        this.$body.addClass("is-new-session is-not-loaded");

        window.onbeforeunload = function () {
          this.$body.css("opacity", 0);
          if (!window.location.hash) {
            window.scrollTo(0, 0);
          }
        };

        this.$loader.css("opacity", 1);

        setTimeout(() => {
          if (!window.location.hash) {
            window.scrollTo(0, 0);
          }
          this.$body.addClass("is-loaded");
          this.$mainContainer.removeClass("is-loading");
          if (this.$video.length) {
            this.$video[0].currentTime = 0;
          }
        }, 2000);
      } else {
        this.$loader.remove();
      }
    } else {
      this.$loader.remove();
      this.$body.removeClass("is-not-loaded");
      this.$body.addClass("is-loaded");
      this.$mainContainer.removeClass("is-loading");
    }
  }
}

const loader = new Loader();
loader.init();
