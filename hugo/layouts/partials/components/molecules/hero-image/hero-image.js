import $ from 'jquery';
import * as utilities from 'global/scripts/utils/utilities';

/**
 *
 */
class HeroImage {

  /**
   *
   */
  init() {
    this.$hero = $('.js-hero-image');
    if (this.$hero.length) {
      this.$intro = this.$hero.find('.js-intro');
      this.$heading = this.$hero.find('.js-heading');
      this.$header = $('.js-header');
      $(window).on('resize', this.onResize.bind(this));

      this.scrollTimeout = 0;
      this.isScrolling = false;
      if (!$('body').hasClass('no-touchevents')) {
        $(window).on('scroll', this.onScroll.bind(this));
      }

      const img = new Image();
      img.src = this.$hero.data('image');
      img.onload = this.onResize.bind(this);
    }
  }

  onScroll() {
    this.isScrolling = true;

    // Clear our timeout throughout the scroll
    window.clearTimeout(this.scrollTimeout);

    // Set a timeout to run after scrolling ends
    this.scrollTimeout = setTimeout(() => {
      this.isScrolling = false;
    }, 66);
  }

  onResize() {
    if (!this.isScrolling) {
      const headerHeight = this.$header.height();
      this.$hero.height(window.innerHeight - headerHeight);
      this.$heading.css('top', `${(window.innerHeight - headerHeight - this.$heading.height()) / 2}px`);
      this.$hero.css('opacity', 1);
      this.$intro.removeClass('is-covered');
    }
  }
}

const heroImage = new HeroImage();
heroImage.init();