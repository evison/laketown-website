import $ from "jquery";
import * as utilities from "global/scripts/utils/utilities";

/**
 *
 */
class CardList {
  /**
   *
   */
  init() {
    this.$cardList = $(".js-card-list");
    this.$cards = $(".no-touchevents .js-card-list .m-card");
    this.$lineupCards = $(".no-touchevents .js-card-list .m-card--lineup");
    this.$headingContainers = this.$cardList.find(".js-heading-container");
    this.$headings = $(".js-card-list .js-heading");

    // utilities.hoverList(this.$lineupCards, 0.7);

    /*$(window).on('resize', this.onResize.bind(this));
    this.onResize();*/
  }

  onResize() {
    utilities.normalizeHeights(this.$cardList, [this.$headingContainers]);
    //utilities.normalizeHeights(this.$cardList, [this.$headings]);
  }
}

const cardList = new CardList();
cardList.init();
