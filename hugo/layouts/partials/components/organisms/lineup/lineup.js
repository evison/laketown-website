import $ from 'jquery';

/**
 *
 */
class Lineup {

  /**
   *
   */
  init() {
    this.$otherArtists = $('.js-other-artists');

    $(window).on('resize', this.onResize.bind(this));
    this.onResize();
  }

  onResize() {
    this.$otherArtists.each((index, section) => {
      let lastElement = false;
      $(section).find('.js-separator').each((index, separator) => {
        $(separator).removeClass('is-hidden');
        if (lastElement && lastElement.offset().top !== $(separator).offset().top) {
          lastElement.addClass('is-hidden');
        }
        lastElement = $(separator);
      });
    });
  }
}

const lineup = new Lineup();
lineup.init();
