import $ from 'jquery';
import Headroom from 'headroom.js/dist/headroom';

/**
 *
 */
class Header {

  /**
   *
   */
  init() {
    this.$header = $('.js-header');

    this.initHeadroom();
  }

  initHeadroom() {
    Headroom.options.tolerance.down = 20; // Scroll tolerance in px
    // vertical offset in px before element is first unpinned
    Headroom.options.offset = 140;
    Headroom.options.onUnpin = () => {
      this.$header.find('.m-nav__item').removeClass('open');
    };

    this.headroom = new Headroom(this.$header[0]);
    this.headroom.init();
  }
}

const header = new Header();
header.init();