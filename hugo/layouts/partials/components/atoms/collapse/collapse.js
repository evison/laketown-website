import $ from 'jquery';

/**
 *
 */
class Collapse {

  /**
   *
   */
  init() {
    $('.js-collapse .js-toggle').on('click', (evt) => {
      evt.preventDefault();

      const $this = $(evt.currentTarget);

      if ($this.next().hasClass('is-open')) {
        $this.next().removeClass('is-open');
        $this.removeClass('is-open');
        $this.next().slideUp(350);
      } else {
        const $toggles = $this.closest('.js-collapse').find('.js-toggle');
        const $panels = $this.closest('.js-collapse').find('.js-inner');
        $toggles.removeClass('is-open');
        $panels.removeClass('is-open');
        $panels.slideUp(350);
        $this.toggleClass('is-open');
        $this.next().toggleClass('is-open');
        $this.next().slideToggle(350);
      }
    });
  }
}

const collapse = new Collapse();
collapse.init();