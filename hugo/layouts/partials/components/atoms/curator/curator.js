import $ from 'jquery';
import * as utilities from 'global/scripts/utils/utilities';

/**
 *
 */
class Curator {

  /**
   *
   */
  init() {
    this.$cards = $('.no-touchevents .js-curator .feed-item');
    utilities.hoverList(this.$cards);
  }
}

const curator = new Curator();
curator.init();