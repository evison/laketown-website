import $ from 'jquery';
import * as utilities from 'global/scripts/utils/utilities';

/**
 *
 */
class Juicer {

  /**
   *
   */
  init() {
    this.$cards = $('.no-touchevents .js-juicer .feed-item');
    utilities.hoverList(this.$cards);
  }
}

const juicer = new Juicer();
juicer.init();