import $ from "jquery";

/**
 * JS is used to check every link within the body, and if the hostname of
 * the href points to an external domain, then a class is added to the
 * anchor. This is only done if the anchor also has the 'a-btn' class,
 * since we wouldn't want to add the icon for inline links.
 */
class ExternalLinks {
  // Static array of hostnames that are not considered external
  static get LOCAL_DOMAINS() {
    return [
      "localhost",
      "laketownranch.com",
      "netlify.com",
      "netlify.app",
      "mailto:",
    ];
  }

  /**
   * Iterates over every anchor, calling the isExternalLink method for each.
   */
  init() {
    $("body")
      .find("a")
      .each((index, el) => {
        if (this.isExternalLink(el)) {
          const $el = $(el);
          $el.addClass("is-external");
          $el.attr("target", "_blank");
        }
      });
  }

  /**
   * Checks if the href points to an external domain for the passed
   * anchor element. Returns true if it does, false otherwise.
   * @param anchor
   * @returns {boolean}
   */
  isExternalLink(anchor) {
    const host = anchor.hostname.toLocaleLowerCase();

    // Catch for href="#", href="tel:555-555-555", etc.
    if (host === "") {
      return false;
    }

    // Check if the hostname contains a substring from any of our local domains
    for (let i = 0; i < ExternalLinks.LOCAL_DOMAINS.length; i++) {
      if (host.indexOf(ExternalLinks.LOCAL_DOMAINS[i]) > -1) {
        return false;
      }
    }
    return true;
  }
}

// Export singleton instance of class
const externalLinks = new ExternalLinks();
externalLinks.init();
