import $ from 'jquery';
import * as utilities from 'global/scripts/utils/utilities';

/**
 *
 */
class HeroCta {

  /**
   *
   */
  init() {
    this.$component = $('.js-hero-cta');
    this.$component.on('click', this.onClick.bind(this))
  }

  onClick() {
    $('html, body').animate({
      scrollTop: window.innerHeight
    }, 1000, 'easeInOutExpo');
  }
}

const heroCta = new HeroCta();
heroCta.init();