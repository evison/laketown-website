import $ from 'jquery';
import * as scrolling from 'global/scripts/utils/scrolling';

/**
 *
 */
class HamburgerBtn {

  /**
   *
   */
  init() {
    this.$hamburger = $('.js-hamburger');
    if (this.$hamburger.length) {
      this.$sidebar = $('.js-sidebar');
      this.$sidebarLinks = this.$sidebar.find('.js-inner');
      this.$links = this.$sidebar.find('.js-nav-link');
      this.$closeLinks = this.$sidebar.find('.js-close-link');
      this.$mainContainer = $('.js-main-container');
      this.$overlay = $('.js-overlay');
      this.$header = $('.js-header');
      this.$logo = this.$header.find('.js-header-logo');

      this.$hamburger.on('click', this.toggleSidebar.bind(this));
      this.$overlay.on('click', this.toggleSidebar.bind(this));
      this.$closeLinks.on('click', () => {
        this.toggleSidebar()
      });
      $(window).on('resize', this.onResize.bind(this));
    }
  }

  toggleSidebar(evt, close = false) {
    if (close) {
      this.$hamburger.removeClass('is-open');
      this.$sidebar.removeClass('is-open');
      this.$header.removeClass('is-menu-open');
      this.$logo.removeClass('is-menu-open');
    } else {
      this.$hamburger.toggleClass('is-open');
      this.$sidebar.toggleClass('is-open');
      this.$header.toggleClass('is-menu-open');
      this.$logo.toggleClass('is-menu-open');
    }

    if (this.$hamburger.hasClass('is-open')) {
      scrolling.disable(this.$sidebar);
      
      this.$sidebarLinks.removeClass('is-open');
      this.$sidebarLinks.css('display', '');

      this.$links.hide();
      this.$links.each((index, el) => {
        const $el = $(el);
        $el.delay(400).fadeIn(200 + index * 190);
      });

      this.$overlay.css({ display: 'block' }).outerWidth(); // Reflow
      this.$overlay.removeClass('is-transparent');
    } else {
      scrolling.enable(this.$sidebar);

      this.$overlay.addClass('is-transparent').one('transitionend', () => {
        this.$overlay.css({ display: '' });
      });
    }
  }

  onResize() {
    if (window.innerWidth >= 980) {
      this.toggleSidebar(null, close);
    }
  }
}

const hamburgerBtn = new HamburgerBtn();
hamburgerBtn.init();