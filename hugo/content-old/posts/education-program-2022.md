---
title: Laketown Ranch Education Program | Laketown Ranch
type: post
layout: post
card_image: 'https://www.datocms-assets.com/64219/1652385642-dji_0185-edit.jpg?w=1260&h=630&q=50&fit=crop&fm=webp'
hero_image: 'https://www.datocms-assets.com/64219/1652385642-dji_0185-edit.jpg?w=1920&h=1080&q=60&fit=crop&fm=webp'
hero_heading: Laketown Ranch Education Program
excerpt: Class of 2022!
slug: education-program-2022
card_tag: LTRanch Education Program 2022
date: 2022-05-02T00:00:00.000Z
share_title: null
share_url: 'https://laketownranch.com/posts/education-program-2022'
share_description: null
share_image: 'https://www.datocms-assets.com/64219/1652385642-dji_0185-edit.jpg?w=1200&h=630&q=60&fit=crop&fm=webp'
---

We’ve just wrapped the inaugural edition of the Laketown Ranch Education Program! After a tremendous response, 30 selected applicants - from the nearly 200 who applied - spent the week at the Ranch learning tricks of the trade.

Students learned about event safety, festival operations, crowd control, tenting, food and beverage, rigging, sound, lighting, audio, lasers and led screens, change overs and more. On the last day they received their boom lift ticket, and took part in a Q&A with industry professionals. 

The weather was mostly… authentic, however the spirits were high with this first graduating class. It was an awesome team, and we are super excited to see them go out into the world with the new skills they’ve acquired.

We hope to see many of the class of 2022 working events around BC this summer!

Many thanks to Creative BC, IATSE and all the instructors for helping to make this happen. It was a huge success.

Stay tuned for info on future editions. 

Below are some photos from the week.


![](https://www.datocms-assets.com/64219/1652385636-dji_0138-edit.jpg?w=1184&q=30)

![](https://www.datocms-assets.com/64219/1652385646-p5a0010-edit.jpg?w=1184&q=30)

![](https://www.datocms-assets.com/64219/1652385650-p5a0005-edit.jpg?w=1184&q=30)

![](https://www.datocms-assets.com/64219/1652385653-p5a9983-edit.jpg?w=1184&q=30)

![](https://www.datocms-assets.com/64219/1652385657-p5a0213-edit.jpg?w=1184&q=30)

![](https://www.datocms-assets.com/64219/1652385661-p5a0198-edit.jpg?w=1184&q=30)

![](https://www.datocms-assets.com/64219/1652385665-p5a0126-edit.jpg?w=1184&q=30)

![](https://www.datocms-assets.com/64219/1652385672-p5a0089-edit.jpg?w=1184&q=30)

![](https://www.datocms-assets.com/64219/1652385668-p5a0116-edit.jpg?w=1184&q=30)

![](https://www.datocms-assets.com/64219/1652385676-p5a0069-edit.jpg?w=1184&q=30)

![](https://www.datocms-assets.com/64219/1652385679-p5a0058-edit.jpg?w=1184&q=30)

![](https://www.datocms-assets.com/64219/1652385682-p5a0031-edit.jpg?w=1184&q=30)

![](https://www.datocms-assets.com/64219/1652385686-laketown-education-apr-30-2022-rms-media-by-rob-porter-7-edit.jpg?w=1184&q=30)

![](https://www.datocms-assets.com/64219/1652385642-dji_0185-edit.jpg?w=1184&q=30)

To stay up to date with future information about the Education Program, and everything else happening at Laketown Ranch [SUBSCRIBE TO OUR NEWSLETTER.](https://app.cyberimpact.com/clients/27247/subscribe-forms/170AC6D6-D9EC-4F19-8490-019FB39D4612)

