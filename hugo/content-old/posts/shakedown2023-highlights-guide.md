---
title: Shakedown Highlights & Guide | Laketown Ranch
type: post
layout: post
card_image: 'https://www.datocms-assets.com/64219/1685654305-ltsd2023-facebook-event_banner-lineup-jun-1920x1005.jpg?w=1260&h=630&q=50&fit=crop&fm=webp'
hero_image: 'https://www.datocms-assets.com/64219/1685654305-ltsd2023-facebook-event_banner-lineup-jun-1920x1005.jpg?w=1920&h=1080&q=60&fit=crop&fm=webp'
hero_heading: Shakedown Highlights & Guide
excerpt: Shakedown Highlights & Guide
slug: shakedown2023-highlights-guide
card_tag: Laketown Shakedown 2023
date: 2023-06-05T00:00:00.000Z
share_title: null
share_url: 'https://laketownranch.com/posts/shakedown2023-highlights-guide'
share_description: null
share_image: 'https://www.datocms-assets.com/64219/1685654305-ltsd2023-facebook-event_banner-lineup-jun-1920x1005.jpg?w=1200&h=630&q=60&fit=crop&fm=webp'
---

Ranchin' During Laketown Shakedown…

# FLATS STAGE


Start your days off with a gang on **Saturday & Sunday** at the little western town stage we call The Laketown Flats. This area is located at the top of Main Street, and is where you can find food, water refill stations, guest services, the box office (for upgrades), a merch booth, and more.  It's also **the** place to catch some of the region's best up-and-comers, and tomorrow next stars. 
On **Saturday, July 1st at 2:15pm**, catch First Nation Hip Hop artist **Xavier Francis**, followed by Cowichan Lake local rockers **Lost Octave** at **3:15pm**.  Finishing off Saturday's Flats program will be the genre-blending sounds of Victoria's own **Blase Blase** at **4:15pm**.

![](https://www.datocms-assets.com/64219/1687640384-ltsd2023-xavier_francis-app-full.jpg)

![](https://www.datocms-assets.com/64219/1687640441-ltsd2023-lost_octave-app-full.jpg)

![](https://www.datocms-assets.com/64219/1687640463-ltsd2023-blase_blase-app-full.jpg)

**Sunday, July 2** will feature the smooth alt-rock stylings of Victoria's **Shale** at **2:15pm**, backed up by Port Renfrew's favourite sons, **Base Collective** at **3:15pm**.  Bringing the big top down at **4:15pm** is the always fantastic funk machine collective, **The New Groovement**!

![](https://www.datocms-assets.com/64219/1687640511-ltsd2023-shale-app-full.jpg)

![](https://www.datocms-assets.com/64219/1687640535-ltsd2023-base_collective-app-full.jpg)

![](https://www.datocms-assets.com/64219/1687640562-ltsd2023-the_new_groovement-app-full.jpg)

Don't sleep on what will be a premium showcase of some of this little island's biggest treasures!


# LAKENIGHT STAGE

![](https://www.datocms-assets.com/64219/1687641820-colin-smith-takes-pics-2021-203-edit.jpg)


**Friday, June 30** - *Rock Royalty*

Opening up our saddle tent Lakenight stage at **5:00pm** will be local artist [Nokturnal Funk](https://soundcloud.com/nokturnalfunk) with his soul-soothing funky tunes before the Victoria eclectic legend [Primitive](https://soundcloud.com/primitivehustle) keeps the party rockin' at **6:15pm**.

![](https://www.datocms-assets.com/64219/1687820348-ltsd2023-nokturnal_funk-app-full.jpg)

![](https://www.datocms-assets.com/64219/1687766250-ltsd2023-primitive-app-full.jpg)

At **11:45**, BC hip-hop Juno nominee [Sonreal](https://soundcloud.com/SONREAL) takes the stage to rock the Shakedown latenight crowd, followed by the always incredible [Vinyl Ritchie](https://soundcloud.com/vinyl-ritchie-2) to close out the first night. What a way to kick things off!

![](https://www.datocms-assets.com/64219/1687820454-ltsd2023-sonreal-app-full.jpg)

![](https://www.datocms-assets.com/64219/1687820461-ltsd2023-vinyl_ritchie-app-full.jpg)

**Saturday, July 1** - *The Year 2000*

[Jennay Badger](https://soundcloud.com/jennaybadger) lights the fuse at **3:15pm** on Saturday with her smooth dance sounds, paving the way for the crowd-favourite returning Shakedown alum [DJ All Good](https://soundcloud.com/peter-poole-1) at **5:00pm**.

![](https://www.datocms-assets.com/64219/1687798162-ltsd2023-jennay_badger-app-full.jpg)

![](https://www.datocms-assets.com/64219/1687798128-ltsd2023-dj_all_good-app-full.jpg)

Following them, local hip hop legend [The Gaff](https://soundcloud.com/thegaff) takes the torch at **6:15pm** to whip the crowd into a froth before the Lakenight stage pauses for the Main stage shows.

![](https://www.datocms-assets.com/64219/1687805968-ltsd2023-the_gaff-app-full.jpg)


'Break Out Artist Of The Year' Juno nominee [Rêve](https://open.spotify.com/artist/06vEAqcicwoSBw85e8biJx) fires the stage back up at **11:45pm** with her soulful and upbeat blend of house & EDM.

Closing off the Saturday night, house music favourite [Nicky Genesis](https://open.spotify.com/artist/1xq1MjEyHp1BpwlxOe8jNM) takes us on a magical dance journey guaranteed to keep your feet moving late into the night. Let's goooo!

![](https://www.datocms-assets.com/64219/1687821115-ltsd2023-reve-app-full.jpg)

![](https://www.datocms-assets.com/64219/1687821122-ltsd2023-nicky_genesis-app-full.jpg)


**Sunday July 2** - *Tropical Barbie World*

With his high energy blend of electronic beats and throbbing basslines, Laketown Ranch staple [GI Blunt](https://soundcloud.com/g-i-blunt) will open up the stage at **3:15pm** for our final day of programming.
After that, Victoria's own techno oddities [Righteous Rainbows Of Togetherness](http://righteousrainbows.com) will land their spaceship at **5:00pm** on planet Shakedown to invade your hearts and command your hips!
House music 'Vibe-Cameleon' [Sivs](https://open.spotify.com/artist/0NUhITSw1R757ncPIm3pGq) is up next with her sexy vocal-driven mix of dance selections, set to keep you moving until the break.

![](https://www.datocms-assets.com/64219/1687816238-ltsd2023-gi_blunt-app-full.jpg)

![](https://www.datocms-assets.com/64219/1687816250-ltsd2023-rrot-app-full.jpg)

![](https://www.datocms-assets.com/64219/1687817430-ltsd2023-sivz-app-full.jpg)

First up after the jump is Mohawk selector and A Tribe Called Red original member [DJ Shub](https://open.spotify.com/artist/3fMA5LH56qpFdPxW1kQe4A) with a not-to-be-missed set when he lights the stage back up at **11:45pm**. After Shub, Vancouver funk/soul/disco DJ [Father Funk](https://open.spotify.com/artist/2v8mmZp35c5E1pTDecZUkv) takes us on a funk-filled journey under the stars.

![](https://www.datocms-assets.com/64219/1687817999-ltsd2023-dj_shub-app-full.jpg)

![](https://www.datocms-assets.com/64219/1687818006-ltsd2023-father_funk-app-full.jpg)


# MAIN STAGE

![](https://www.datocms-assets.com/64219/1687818154-ltsd2022-main-arkells-credit_colin-smith.jpg)


**Friday, June 30** - *Rock Royalty*

The main festival bowl party kicks off when **gates open at 3pm**. 

A winner of the 2023 "Play At Shakedown" video submission contest [Darrian Gerard](https://open.spotify.com/artist/149qI3YQHoTrM0oSimXTqf) opens the weekend's main stage performances at **4:10pm** with her infectuous, silky pop-rock offerings, followed by Victoria's high-octane alt-rock up and comers, [Wet Future](https://open.spotify.com/artist/720XvVpEV8TRpBtCeCCke1) at **5:00pm**.

![](https://www.datocms-assets.com/64219/1687819878-ltsd2023-darriangerard-full.jpg)

![](https://www.datocms-assets.com/64219/1687819886-ltsd2023-wet_future-app-full.jpg)

A not-to-be-missed performance at **6:15pm** from [Blonde Diamond](https://open.spotify.com/artist/0skYMbISqV2drnQbJopK8Y) will have you singing and dancing along to their tasty blend of rock, r&b, disco & electro-pop. Next up is the indelible and multi-disciplinary Canadian icon, [Bif Naked](https://open.spotify.com/artist/02odAcSXGSPTSO4P44Ztuw?autoplay=true) at **7:30pm**.
Seattle indie and blues rock trio [Reignwolf](https://open.spotify.com/artist/66YGDwn22fjphzqGCSIbbK?autoplay=true) will shred their way into your hearts and souls at **9:00pm**.

![](https://www.datocms-assets.com/64219/1687820095-ltsd2023-blonde_diamond-app-full.jpg)

![](https://www.datocms-assets.com/64219/1687820102-ltsd2023-bif_naked-app-full.jpg)

![](https://www.datocms-assets.com/64219/1687820107-ltsd2023-reignwolf-app-full.jpg)

Then, make sure to step back from that ledge, because closing the main stage at **10:30pm** on Friday night is the San Franciscan rock mega-stars, [Third Eye Blind](https://open.spotify.com/artist/6TcnmlCSxihzWOQJ8k0rNS?autoplay=true) offering up what will be a nostalgic singalong that you will not want to miss!

![](https://www.datocms-assets.com/64219/1687821167-ltsd2023-third_eye_blind-app-full.jpg)


**Saturday, July 1** - *The Year 2000*

At **4:10pm**, the local [Quw’utsun’ Tzinquaw Dancers](https://www.youtube.com/watch?v=LL7xJQ6U2Yo&ab_channel=SAGAcomProductions) will open the main stage with an incredible and powerful performance, before the super talented pop minded art-rockers [Pastel Blank](https://open.spotify.com/artist/4v8MCErhmubUGQhwRVbnlN?autoplay=true) gets the crowd doing their own dance of joy at **5:00pm**.

![](https://www.datocms-assets.com/64219/1687822162-ltsd2023-quwutsun-full.jpg)

![](https://www.datocms-assets.com/64219/1687822170-ltsd2023-pastel_blank-app-full.jpg)

At **6:15pm**, Montreal electro-soul and hip hop collective [Busty And The Bass](https://open.spotify.com/artist/4XMc1qHObZ7aXQrH5MmbjK?autoplay=true) will blow the house down with their diverse and huge sound.  Next to light up the big stage is Vancouver indie-rock mainstays [Yukon Blonde](https://open.spotify.com/artist/3CdvcTOH01EXzXu96afkSN?autoplay=true) at **7:30pm**.
Hip hop's Somali-Canadian Juno recipient [K'Naan](https://open.spotify.com/artist/7pGyQZx9thVa8GxMBeXscB?autoplay=true) brings his songwriting and lyrical talents to the Shakedown crowd in what will be a set you will not want to miss at **9:00pm**!

![](https://www.datocms-assets.com/64219/1687823448-ltsd2023-busty_and_the_bass-app-full.jpg)

![](https://www.datocms-assets.com/64219/1687823455-ltsd2023-yukon_blonde-full.jpg)

![](https://www.datocms-assets.com/64219/1687824058-ltsd2023-knaan-app-full.jpg)

At **10:30pm**, Portland Grammy winners [Portugal. The Man](https://open.spotify.com/artist/4kI8Ie27vjvonwaB2ePh8T?autoplay=true) take us on a journey with their signature psychedelic rock sound to close the main stage on Saturday.

![](https://www.datocms-assets.com/64219/1687826056-ltsd2023-portugal_the_man-app-full.jpg)


**Sunday, July 2** - *Tropical Barbie World*

On Sunday at **4:10pm**, another winner of this year's "Play At Shakedown" contest, [Sirreal](https://open.spotify.com/artist/6umvpaTwCk83m9jNjLyvXi?autoplay=true) will bring his blend of hip hop and party rock to the stage. At **5:00pm**, crowd-favourite bilingual rapper [Missy D](https://open.spotify.com/artist/3a0T1urq8A1WVbsQq7X5K1?autoplay=true) will have you catching all sorts of feels with her super smooth spits and soulful voice.  Speaking of soul, [The Boom Booms](https://open.spotify.com/artist/5JxHwNQESJ2ttqwPce1Ncq?autoplay=true) are up next at **6:15pm**, serving up a chalk-full plate of latin funk inspired afropop.  Hope your ears are hungry!

![](https://www.datocms-assets.com/64219/1687827609-ltsd2023-sirreal-full.jpg)

![](https://www.datocms-assets.com/64219/1687827626-ltsd2023-missy_d-app-full.jpg)

![](https://www.datocms-assets.com/64219/1687827636-ltsd2023-the_boom_booms-app-full.jpg)

At **7:30pm**, the party continues with Nova Scotian rapper and producer [Classified](https://open.spotify.com/artist/7t6GsqGAwrj1kwYbvNX0hN?autoplay=true) with what promises to be a hype-enducing hip hopstravaganza.

![](https://www.datocms-assets.com/64219/1687834082-ltsd2023-classified-app-full.jpg)

Then get ready for the fantastic life in plastic Danish-Norwegian dance-pop group, [Aqua](https://open.spotify.com/artist/6kBjAFKyd0he7LiA5GQ3Gz?autoplay=true) at **9:00pm**!  Sunday's theme is Tropical Barbie World, so we expect to see heaps of you Kens and Barbies singing along!

![](https://www.datocms-assets.com/64219/1687834527-ltsd2023-aqua-app-full.jpg)

At **10:30pm** on Sunday night, Mr. Boombastic himself, the one whom it wasn't, [Shaggy](https://open.spotify.com/artist/5EvFsr3kj42KNv97ZEnqij?autoplay=true) closes the main stage for the 2023 edition of Laketown Shakedown.  Do not miss this Juno & multiple Grammy award winner on the most beautiful stage in western Canada. What a way to punctuate the weekend!

![](https://www.datocms-assets.com/64219/1687834889-ltsd2023-shaggy-app-full.jpg)

Looking forward to seeing you beauties this weekend!

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/playlist/2QhROp3MhaZLc0NcIBJVKY?utm_source=generator" width="100%" height="352" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

![](https://www.datocms-assets.com/64219/1687757324-ltsd2023-schedule-all-web.jpg)
