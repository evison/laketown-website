---
title: Play At Laketown Ranch! | Laketown Ranch
type: post
layout: post
card_image: 'https://www.datocms-assets.com/64219/1681257686-ltr-play_at-2023.jpg?w=1260&h=630&q=50&fit=crop&fm=webp'
hero_image: 'https://www.datocms-assets.com/64219/1681257686-ltr-play_at-2023.jpg?w=1920&h=1080&q=60&fit=crop&fm=webp'
hero_heading: Play At Laketown Ranch!
excerpt: Ever dreamt of playing at Laketown Ranch?
slug: play-at-laketown-ranch-2023
card_tag: Laketown Ranch 2023
date: 2023-04-12T00:00:00.000Z
share_title: null
share_url: 'https://laketownranch.com/posts/play-at-laketown-ranch-2023'
share_description: null
share_image: 'https://www.datocms-assets.com/64219/1681257686-ltr-play_at-2023.jpg?w=1200&h=630&q=60&fit=crop&fm=webp'
---

# **ATTENTION MUSICAL ACTS!**

**PLAY LAKETOWN CONTESTS**


Ever dreamt of playing at Sunfest or Laketown Shakedown?
**NOW'S YOUR CHANCE!**

**HERE'S HOW IT WORKS:**

Upload a performance video via the links below, share the link and get all your friends, family and fans to vote for you!

All types of musical acts are accepted!

The acts with the **most votes** per event wins a chance to play the **LAKENIGHT STAGE** at Sunfest Country, or the **MAIN STAGE** at Laketown Shakedown!

And the best part is, **everyone who votes** will be entered to **WIN 2 UPGRADES** to our brand new area, the **LOOKOUT LOUNGE!**

Contest closes April 26 @ 5pm

*Max upload size is 300mb*

To submit your video, and to vote on your favourite submission, click and share the links below!

[![](https://www.datocms-assets.com/64219/1681258819-sf2023-tbits-playatsunfest-1920x1048.png)](https://sunfestcountry.tbits.me/tb_app/485390)

[![](https://www.datocms-assets.com/64219/1681257905-ltsd2023-tbits-playatshakedown-1920x1048.png)](https://contest.laketownranch.com/tb_app/485413)
