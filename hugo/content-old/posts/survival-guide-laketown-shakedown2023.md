---
title: Survival Guide - Laketown Shakedown | Laketown Ranch
type: post
layout: post
card_image: 'https://www.datocms-assets.com/64219/1688062492-colin-smith-takes-pics-2021-222-edit.jpg?w=1260&h=630&q=50&fit=crop&fm=webp'
hero_image: 'https://www.datocms-assets.com/64219/1688062492-colin-smith-takes-pics-2021-222-edit.jpg?w=1920&h=1080&q=60&fit=crop&fm=webp'
hero_heading: Survival Guide - Laketown Shakedown
excerpt: Features & Key Things To Remember
slug: survival-guide-laketown-shakedown2023
card_tag: Laketown Shakedown 2023
date: 2023-06-30T00:00:00.000Z
share_title: null
share_url: 'https://laketownranch.com/posts/survival-guide-laketown-shakedown2023'
share_description: null
share_image: 'https://www.datocms-assets.com/64219/1688062492-colin-smith-takes-pics-2021-222-edit.jpg?w=1200&h=630&q=60&fit=crop&fm=webp'
---

Today is the day!  Laketown Ranch opens its doors at 3pm for Laketown Shakedown 2023!

![](https://www.datocms-assets.com/64219/1688059253-colin-smith-takes-pics-2021-73-1-edit.jpg)

To better prepare you for your weekend of fun and frolic at the Ranch, we've put together some reminders to help you find your way around.
Give a read through and get familiar with the accessible areas. 

# Lower Camping Area & Flats
The lower camping area being used for all camping this edition.  This area is organized into different sections & street names.  Looking up Main Street from the parking area, you have your streets on the left of Main, and your roads to the right.  They're all named after musicians, to better help you remember where you and your gang have all set up camp.

![](https://www.datocms-assets.com/64219/1688148584-ltsd2023-fullsitemap-lower-4800.jpg)

Up at the top of Lower Camping, is the Laketown Flats.  The Flats stage is playing host to some amazing local talent during matinee shows on Friday & Saturday.  Be sure to check the schedule! You'll also just want to head to this area in general for food, drink, washrooms, showers, to use as an easy meetup point, or just for some general respite.  Have a question?  Need a hat?  Hungry?  Out of cash?  Get a sliver?  First aid, merchadise, food vendors, ATMs, festival information, as well as the RCMP office are all found in The Flats area.  Showers and flush toilets can also be found in this zone.

# Festival Bowl & Main Stage
A lot of action going on in the bowl!  Hungry?  Thirsty?  Gotta pee?  You'll find food vendors, a bar, water, accessible washrooms, and smoking sections along the west side of the bowl.  Is your favourite artist playing and you want to get closer to it all?  VIP, Lookout Lounge and Lawn/Stage Pit ticket upgrades are available in the upper centre of the bowl.  Also a good place to grab some merch to wear once you get up to the front!  Merchardise booths are found in this area as well, along with ATMs.  Want to take a break and watch from above?  Head on over to the Hillside on the east side of the bowl to take in the sights and sounds from the knoll.  The Family Zone is also on the Hillside.  So is the zipline!

![](https://www.datocms-assets.com/64219/1653975278-sunday-laketown-shakedown-colin-smith-takes-pics-2019-124.jpg)

![](https://www.datocms-assets.com/64219/1688112768-ltsd2023-fullsitemap-festival_bowl-2400.jpg)



# Lakenight Stage
While you're on the Hillside, note the Lakenight Stage at the tippy top of the bowl area.  You'll be coming back here later.  Sooner than later in fact.  Check the schedule.  Set some alarms, and watch your watch.  There's food, water, washrooms, all the amenities you need up here too.

![](https://www.datocms-assets.com/64219/1687641820-colin-smith-takes-pics-2021-203-edit.jpg)


# Shuttles
There is shuttle service available to and from Duncan, as well as Lake Cowichan.

All shuttle information can be found on our website here:[https://www.laketownshakedown.com/getting-here/](https://www.laketownshakedown.com/getting-here/)

**Please note that traffic/road conditions may affect shuttle schedule.


# Quick But Important Notes
* DRINK WATER!
* WEAR SUNGLASSES!
* SUNSCREEN TOO!
* USE THE BUDDY SYSTEM!
* KIDS LOVE EARPLUGS!
* NO READMISSION TO THE CONCERT BOWL AFTER 8PM!
* KNOW YOUR LIMITS, STAY WITHIN THEM!
* DON'T LITTER!
* STAY OFF THE **SIN WAGON**!
* WATCH YOUR FOOTING!
* BE NICE!
* MAKE NEW FRIENDS!
* HAVE FUN!
* STEP BACK FROM THAT LEDGE MY FRIEND!

The property is home to all sorts of wildlife.  We've seen herds of elk each night this week, and a bear, which is amazing, but it's also a good reminder that we are guests here, and we all need to respect that this is their home.  Pick up your trash, don't put ciggy butts on the ground, and do not approach the wildlife.. Even if you're almost positive that it's just someone dressed in a furry wolf costume.


**Early Bird Camping**
Early bird load in today starts at 1pm.
Show up **on time** to your slot!  This doesn't mean early!  It's important to not come before your selected entry time, as you won't be granted access until your load in time, and parking on the highway is not permitted!

# Schedule & App
Plan your day accordingly so you don't miss your favourite artists!  Keep eyes on the schedule!  There are Info Hubs sprinkled around the site that include the schedule, maps, and key info.  There's also a QR code to download the Laketown Ranch App, allowing you to keep all the info in your pocket.  The QR code looks like this:

![](https://www.datocms-assets.com/64219/1656450400-laketown_ranch_app_page-300.jpg)

![](https://www.datocms-assets.com/64219/1687757324-ltsd2023-schedule-all-web.jpg)
