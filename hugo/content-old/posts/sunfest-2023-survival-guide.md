---
title: Sunfest 2023 Survival Guide | Laketown Ranch
type: post
layout: post
card_image: 'https://www.datocms-assets.com/64219/1690263898-tfalkchalmers-522-1920x1280.jpg?w=1260&h=630&q=50&fit=crop&fm=webp'
hero_image: 'https://www.datocms-assets.com/64219/1690263898-tfalkchalmers-522-1920x1280.jpg?w=1920&h=1080&q=60&fit=crop&fm=webp'
hero_heading: Sunfest 2023 Survival Guide
excerpt: Find your way around Sunfest!
slug: sunfest-2023-survival-guide
card_tag: Sunfest 2023
date: 2023-07-28T00:00:00.000Z
share_title: null
share_url: 'https://laketownranch.com/posts/sunfest-2023-survival-guide'
share_description: null
share_image: 'https://www.datocms-assets.com/64219/1690263898-tfalkchalmers-522-1920x1280.jpg?w=1200&h=630&q=60&fit=crop&fm=webp'
---

Just a week remains until the biggest country music fesival in BC kicks the barn doors open and welcomes you all back to Laketown Ranch!

This year is poised to be one of the biggest and best editions of Sunfest ever with an amazing lineup of artists, and numerous exciting improvements and additions to the property.

![](https://www.datocms-assets.com/64219/1690262684-tfalkchalmers-110-1920.jpg)

Read on for some useful tips and a guide to help you enjoy your weekend to its fullest!

# Arrive On Time And Prepared!
Please have all your documents ready once you arrive, so that our ingress team can get you swiftly through the gates and into your spot.  This includes ID, printed tickets, e-tickets, vouchers, etc.  If you are joining us for early entry camping, it's important that you **do not arrive before your booked time**.  We can only load in a certain number of campers per hour, so please show up on time for your booking!


# Lower Camping Area
It's expansive, and organized into different sections & street names.  Looking up Main Street from the parking area, you have your streets on the left of Main, and your roads to the right.  They're all named after musicians, to better help you remember where you and your gang have all set up camp.  Meet your neighbours!  Go for walk through the lanes and check out all the dressed up campsites and activities.  At the top of lower camping by the Mellor sites you'll find the wedding chapel, slip and slide, and other activities.

![](https://www.datocms-assets.com/64219/1690263619-sf2023-sitemap-lower-1920.jpg)


# Laketown Flats
Up at the top of Lower Camping, before the main stage bowl, is the Laketown Flats.  You'll want to head to this area for upgrades, guest services, lost & found, food, drink, flush toilets, showers, to use as an easy meetup point, or just for some general shady respite.  The Flats stage is playing host to some amazing local talent throughout the weekend, as well as a BC Country Music Association showcase Friday through Sunday.  Be sure to check the schedule!  Also line dancing, and music poker starting at 11:45 takes place on the Flats stage.

Have a question?  Need a hat?  Hungry?  Out of cash?  Get a sliver?  First aid, merchadise, food vendors, ATMs, festival information, as well as security and the RCMP office are all found in The Flats area.


# Festival Bowl & Main Stage
A lot of action going on in the bowl!  Hungry?  Thirsty?  Gotta pee?  You'll find food vendors, a bar, water, accesible washrooms, and smoking sections along the west side of the festival bowl.  Is your favourite artist playing and you want to get closer to it all?  VIP and Stage Pit ticket upgrades are available in the upper centre of the Hillside.  This is also a great place to grab some merch to wear once you get up to the front!  Merchardise booths are found in this area as well, along with ATMs.  Want to take a break and watch from above?  Head on over to the top of the Hillside on the east side of the bowl to take in the sights and sounds from the knoll.  The Family Zone is also on the Hillside.  So is the zipline!

![](https://www.datocms-assets.com/64219/1690499971-tfalkchalmers-329-1920.jpg)

![](https://www.datocms-assets.com/64219/1690500999-sf2023-sitemap-bowl-2400.png)

# Lookout Lounge
Our newest area in the festival bowl, the Lookout Lounge sits above the crowd on the east side of the Stage Pit area.  Here you can enjoy immaculate views of the mainstage, while sitting (or standing) at your own table under shade structures, dedicated flush toilets, complimentary drinks and hors d'oeuvres, access to the Lawn & Hillside areas, souvenir lanyard & more!  Upgrade your ticket to the lookout Lounge by visiting the box office!


# Lakenight Stage
Up at the tippy top of the bowl area, you'll find the Lakenight Stage.  You'll be coming back here later.  Once the main stage closes for the day, the party continues under the saddle tent!  Check the schedule, set some alarms, and watch your watch!  There's food, water, washrooms, all the amenities you need up here too.

![](https://www.datocms-assets.com/64219/1690588956-tfalkchalmers-235-1920.jpg)


# Upper Camping
You staying up in the top of the property?  Then you need to know your way around!  Dissimilarly to the U2 song, here the streets do in fact have names.  Remember to remember which streetname you're on, and check out the maps to find your way home!  You'll also find the Hilltop Hub in upper camping, which provides you with flush toilets, hot showers, & delicious water to fill your bottles!  

![](https://www.datocms-assets.com/64219/1690589375-sf2023-sitemap-upper-1920.png)

Also up at the top of the property are a web of different hiking and biking trail routes.  The vistas at the top of the plateau are amazing, so why not go for a little walk during the day?  You'll be happy to you did!


# Shuttles
There is shuttle service available to and from Duncan, as well as Lake Cowichan.
Check out the Getting Here page on our website for the schedule:
[sunfestconcerts.com/getting-here](https://https://www.sunfestconcerts.com/getting-here/)

Find all shuttles at **GATE 1**

**Private Shuttles and Hotel PickUps:**

Private shuttle service to and from Youbou, Lake Cowichan & Honeymoon Bay provided in part by MyGo Tours and Transportation, Trips For Trips and This Rides for You.
Shuttles from Langford provided by Cascadia Tours. Shuttles from Victoria & Sidney offered by Freedom Adventure Bus Society. Reservations are recommended.

For more information, visit:

MYGO TOURS AND TRANSPORTATION
[mygo.ca](https://mygo.ca) or [cheerscowichan.com](https://cheerscowichan.com)
Or call 250-732-1120

This Rides For You (Accessible)
Call 250-709-7772

Freedom Adventure Bus Society (Accessible, operating from South Island)
Call 250-508-1808

Cascadia Tours (Operating from Langford)
Call 250-818-7521

Duncan Taxi
Call 250-746-4444

Trips For Tips
Call 250-210-0518

**Please note that traffic/road conditions may affect shuttle schedule.


# Quick But Important Notes
* DRINK WATER!
* WEAR SUNGLASSES!
* SUNSCREEN TOO!
* USE THE BUDDY SYSTEM!
* KIDS LOVE EARPLUGS!
* NO READMISSION TO THE CONCERT BOWL AFTER 8PM!
* KNOW YOUR LIMITS, STAY WITHIN THEM!
* DON'T LITTER!
* STAY OFF THE **SIN WAGON**!
* WATCH YOUR FOOTING!
* BE NICE!
* MAKE NEW FRIENDS!
* HAVE FUN!
* YEE HAW!

The property is home to all sorts of wildlife.  Remember that we are guests here, and we **all** need to respect that this is their home.  Pick up your trash, don't put ciggy butts on the ground, and do not approach the wildlife.  


**Early Bird Camping**
Early bird load in starts at 1pm on Wednesday and runs from 1pm - 9pm on Thursday.
Show up on time to your slot!  This doesn't mean early!  It's important to not come before your selected entry time, as you won't be granted access until your load in time, and parking on the highway is not permitted!

# Schedule & App
Plan your day accordingly so you don't miss your favourite artists!  Keep eyes on the schedule!  There are Info Hubs sprinkled around the site that include the schedule, maps, and key info.  There's also a QR code to download the Laketown Ranch App, allowing you to get push notification updates throughout the weekend.
The QR code looks like this (click to visit the mobile app page on our website to download the app):

[![](https://www.datocms-assets.com/64219/1656450400-laketown_ranch_app_page-300.jpg)](https://www.laketownranch.com/mobile-app/)

[![](https://www.datocms-assets.com/64219/1690594732-sf2023-schedule-17x11-web.png)](https://www.sunfestconcerts.com/schedule/)