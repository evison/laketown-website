---
title: Early Ticket Buyers Prize Packages! | Laketown Ranch
type: post
layout: post
card_image: 'https://www.datocms-assets.com/64219/1661852627-tfalkchalmers-328.jpg?w=1260&h=630&q=50&fit=crop&fm=webp'
hero_image: 'https://www.datocms-assets.com/64219/1661852627-tfalkchalmers-328.jpg?w=1920&h=1080&q=60&fit=crop&fm=webp'
hero_heading: Early Ticket Buyers Prize Packages!
excerpt: Win HUGE prize bundles!
slug: boombastic-giveaway
card_tag: Sunfest & Shakedown
date: 2023-04-20T00:00:00.000Z
share_title: null
share_url: 'https://laketownranch.com/posts/boombastic-giveaway'
share_description: null
share_image: 'https://www.datocms-assets.com/64219/1661852627-tfalkchalmers-328.jpg?w=1200&h=630&q=60&fit=crop&fm=webp'
---

# GIVEAWAY TIME!

We're ramping up the prizing on this one!
Purchase your **full weekend or weekend warrior** tickets by the dates below to either Sunfest or Laketown Shakedown and be entered to **WIN** one of three awesome packages!

Already bought your tickets?  Well then you're already entered!

# PRIZE PACKAGES:
**PURCHASE BY MAY 8 (GRAND PRIZE):**
* 6 LOOKOUT LOUNGE UPGRADES
* $200 Merch Package
* Backstage Tour for 6
* Private Soundcheck Party in Front of House Owners Suite (Drinks included)
* 6 Weekend Zipline Passes

*Value
$4300 (Shakedown) / $4900 (Sunfest)*


**PURCHASE BY MAY 22**
* 4 LOOKOUT LOUNGE UPGRADES
* $150 Merch Package
* Backstage Tour for 4
* 4 Weekend Zipline Passes

*Value
$2800 (Shakedown) / $3200 (Sunfest)*

**PURCHASE BY JUNE 5**
* 2 LOOKOUT LOUNGE UPGRADES
* $100 Merch Package
* 2 Weekend Zipline Passes

*Value
$1300 (Shakedown) / $1500 (Sunfest)*

All three prize bundles include weekend upgrades to our brand new for 2023 area, the **LOOKOUT LOUNGE!**
A new premium concert experience at Laketown Ranch.



**WHAT TO EXPECT IN THE LOUNGE:**

* Elevated sight lines from our inner bowl’s new elevated lounge area
* Shade
* Seated and standing options
* Lounge only flush toilets
* Private bar with 3 drinks per night included
* Non-alcoholic options included
* Hors d'oeuvres
* Souvenir lanyard


**Already bought your tickets?**
Well then great news, you’re already entered!

**Winners will be announced at each milestone.
Don’t sleep! Get your tickets today!**

[![](https://www.datocms-assets.com/64219/1682105600-sd2023-blog-buytixandwin-1200x630.jpg)](https://laketownshakedown.frontgatetickets.com/)

[![](https://www.datocms-assets.com/64219/1682105533-sf2023-blog-buytixandwin-1200x630.jpg)](https://sunfest.frontgatetickets.com/)