---
title: Laketown Ranch Benevolent Society | Laketown Ranch
type: page
layout: benevolent
hero_image: /static/images/hero-benevolent.jpg
hero_heading: Laketown Ranch Benevolent Society
---

{{< h2-underlined "Laketown Ranch Benevolent Society" >}}

Giving back to the community is part of who we are at Laketown Ranch, and will continue to be a measure of our success. It is so important in fact, that we have an incorporated benevolent society. Our legacy of community alliance spans over a decade, donating to over 50 charities, non-profits, schools, community partners and causes.

The purposes of the society are:

1. To promote the performance, production, and appreciation of live music performances in the Cowichan Valley and on Vancouver Island; and
2. To assist health, education, and arts related groups in the Cowichan Valley and Vancouver Island by providing monetary and in-kind donations.

Through the years, we have raised over $1,000,000, benefitting vital organizations in our community, including:

- The Cowichan District Hospital Foundation
- MS Society, Canadian Diabetes Association
- Cops for Cancer Tour de Rock
- Big Brothers Big Sisters
- Rotary Club of Duncan
- Radio Cowichan
- Cowichan Valley Performing Arts Foundation
- Music Heals
- Duncan Chamber of Commerce
- Roughly twenty elementary, middle and high schools in the area.

We also contribute to the economic, social, cultural and agricultural growth of the Cowichan Valley, having infused over 10 million dollars into local business through direct and indirect investment and employment opportunities since our inception.

This year, the Laketown Ranch Benevolent Society will be extending its scope to incorporate potential fundraising opportunities through event beverage gardens, raffles, and ticket fees at Laketown Ranch.

Together, we are positively shaping the community of the Cowichan Valley, and that’s something we can all stand behind.

If you are a non-profit organization seeking a donation, please send us an official request letter, on letterhead, with details of how your event or organization benefits the community. Thank you.

{{<benevolent-gallery>}}

{{<br>}}

{{<br>}}

{{< h2-underlined "Current Projects" >}}

##### [Lake Cowichan Food Bank](https://www.facebook.com/LakeCowichanFoodBank/)

A few days before Christmas, we launched a 2-day donation drive for the Lake Cowichan Food Bank. Every person who donated $75 or more received 2 Thursday night tickets to Sunfest 2018. In addition, we matched the first 10 donations, totalling $1,975. The grand total of the donation drive was $5,050!

We were amazed by the generosity of our community and are looking forward to supporting this essential organization again in the future.

{{<br>}}

##### [Radio Cowichan](http://www.cowichanvalleycitizen.com/community/video-radio-cowichan-wins-1000-in-laketown-ranch-contest/)

In May 2017, we launched the contest, ‘Help Us Help Others’, where we asked our social media followers to nominate local organizations that do amazing work in our community, to win a $1,000 donation from the Laketown Ranch Benevolent Society.

After receiving dozens of nominations, we chose Radio Cowichan for the donation because of their unyielding support of local children. Not only do their programs teach these kids the technical aspects of broadcasting, they also instill in them self confidence, self esteem and the courage to use their voice.

{{<br>}}

##### [Cowichan Valley Performing Arts Foundation](https://www.youtube.com/watch?v=RhQALo7eXfw)

In the Spring of 2017, the Laketown Ranch Benevolent Society was honoured to present scholarships to 5 young, local musicians through the CVPAF, to help them pursue their musical passions.
