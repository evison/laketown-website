---
title: Mobile App | Laketown Ranch
type: page
layout: page
hero_image: /static/images/hero-app.jpg
hero_heading: Laketown Ranch Mobile App
no_app_banner: true
---
#### Download
{{< app-promo >}}

The Laketown Ranch mobile app is the only place to get the full events schedule, GPS site map and artist music previews.

Download our mobile app to also explore our other events, purchase tickets and get the latest updates.


#### Features

* Tickets - Purchase passes, camping and more
* Events - Instant access to information on Sunfest Country and Laketown Shakedown
* Lineup - Check out artist bios, and listen to music previews
* Camping - All you need to enjoy your nights with us

![alt text](/static/images/app-3up.jpg "Laketown Ranch Mobile App")
