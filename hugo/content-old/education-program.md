---
title: Laketown Ranch Education Program | Laketown Ranch
type: page
layout: page
hero_image: https://www.datocms-assets.com/64219/1646698524-education-program-image1.jpg?q=60&w=1920&h=1080
hero_heading: Laketown Education Program
---

<div><img style="margin-bottom: 3rem;" src="/static/images/laketown-ranch-education-program.svg" alt="Laketown Ranch Education Programe" style="display:block;margin:auto;" /></div>

Have you ever been curious about what goes on behind the scenes at festivals, concerts and events? Have you ever thought it would be fun to join a team that produces them? Have you ever considered a career in the Live Event industry but didn’t know where to gain the skills? If you gave an enthusiastic “Yes” to any of those then the “Laketown Education Program” may be for you.

## What is it?

The Laketown Education Program, **April 25 - 30, 2022**, is a 6-day hands-on program for people hoping to get some experience and opportunities in the live event industry. This diverse experience will have you learn event safety, operations, sound and light production, crowd management, tenting, machinery, and other key skills for large scale concerts and events from current industry professionals.

All of this will take place at one of Canada’s largest outdoor venues, Laketown Ranch - home to Sunfest, Laketown Shakedown and more.

## What will it be like?

Each day will be structured around a 10-hour day at Laketown ranch broken into four 2-hour lessons taught by industry professionals. Lessons will contain both a theory and a hands on component. The course structure will be as follows;

- 2 Days of Festival Site Operations
- 2 Days of Stage Lighting and Sound Production
- 1 Day Machinery Training - Boom Lift Ticket Provided on Successful Assessment
- 1 Day Event Safety and Stakeholder Panel

## What happens after the course?

Following the completion of the course, attendees will be connected with event producers around B.C. with help from the BC Music Festival Collective. Armed with their new skills, we are hopeful that many graduates will be successfully employed for the 2022 season and beyond.

## How do I apply?

Merely fill out this short questionnaire detailing why you should attend [CLICK HERE](https://docs.google.com/forms/d/1H-11L-LRZV7wBNtYJkO72sDYwShUUN7UbRTbqLTzyfo/viewform). Limited spots are available, so put your best foot forward.

Successful applicants will be notified by April 4th.

## What does it cost?

Thanks to the support from Creative BC, the Province of BC and our other sponsors, the first year of this program is free to successful applicants. Transportation to the venue, food and beverages are not included.

## Where will I stay?

All successful applicants are welcome to camp at Laketown Ranch Campgrounds, free of charge. If this doesn't suit you, there are many other accommodation options in the Cowichan Valley region.

Any questions, please hit us up at [info@laketownranch.com](mailto:info@laketownranch.com).

We look forward to seeing you on the Ranch!

<div style="max-width:600px;margin:40px auto;">
<img src="https://www.datocms-assets.com/64219/1646869932-creativebc_logo_2022_cmyk_creativebc_logopinkc_bc.svg" alt="Creative BC, British Columbia">
<img src="https://www.datocms-assets.com/64219/1648663092-168_logo_solid_fullsize.png?q=60&w=1184&h=784&fm=webp" alt="" style="display:block;max-height:250px;margin:30px auto;">
</div>

![](https://www.datocms-assets.com/64219/1646698510-education-program-image2.jpg?q=60&w=1184&h=784&fm=webp)
