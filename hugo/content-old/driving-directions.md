---
title: Driving Directions | Laketown Ranch
type: page
layout: directions
hero_heading: Driving Directions
---
# Driving Directions

**Laketown Ranch is located at 648 - 8811 Youbou Rd, Cowichan Lake, British Columbia.**

#### From North (Nanaimo Ferries or Airport)
* Head south on Highway 1
* Continue until Highway 18, just north of Duncan
* Turn right on to Highway 18 heading west towards Lake Cowichan
* Veer right on to Youbou Road (Don't turn left towards the shops in Lake Cowichan)

#### From South (Victoria Ferries or Airport)
* Head south on Highway 17
* Follow signs for Nanaimo and head north on Highway 1
* Continue until Highway 18, just north of Duncan
* Turn left on to Highway 18 and head west towards Lake Cowichan
* Veer right on to Youbou Road (Don't turn left towards the shops in Lake Cowichan)

#### From Vancouver
* From Horseshoe Bay (West Vancouver) or Tsawwassen (south of Vancouver), take a ferry bound for Nanaimo or Victoria
* Follow our driving directions from Nanaimo or Victoria ferries above