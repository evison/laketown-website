---
title: Contact Us | Laketown Ranch
type: page
layout: page
hero_image: /static/images/hero-contact-us.jpg
hero_heading: Contact Us
contact_form: true
contact_heading: Contact Form
---
#### For General Inquiries
[info@laketownevents.com](mailto:info@laketownevents.com)

Please note: due to the high volume of emails we receive, we will not respond to emails regarding buying or selling tickets on third party sites.

#### For Camping Related Inquiries
Contact us at [campground@laketownevents.com](mailto:campground@laketownevents.com) or [250-710-5868](tel:250-710-5868)

#### Volunteer Information
For further questions about volunteering please email [volunteer@laketownevents.com](mailto:volunteer@laketownevents.com)

#### Media Inquiries
For all media inquiries please contact our PR Manager at: [media@laketownevents.com](mailto:media@laketownevents.com)

#### Our Address
8811-2 Youbou Rd<br>
Lake Cowichan, BC<br>
V0R 2G1<br>
Canada<br>
