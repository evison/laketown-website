const { src, dest } = require("gulp");
const svgSprite = require("gulp-svg-sprite");
const cfg = require("../config").svg;
const streamSize = require("../util/streamsize");

const svgSpriteConfig = {
  transform: ["svgo"],
  mode: {
    symbol: {
      dest: ".",
      sprite: "sprite.symbol.svg",
    },
  },
};

function svg(cb) {
  return src(cfg.sourcePath)
    .pipe(svgSprite(svgSpriteConfig))
    .pipe(streamSize("SVG"))
    .pipe(dest(cfg.destinationPath));
  cb();
}

function svgClean(cb) {
  del([cfg.destinationPath]);
  cb();
}

exports.svg = svg;
exports.svgClean = svgClean;
