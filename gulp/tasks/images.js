const del = require("del");
const { src, dest } = require("gulp");
const imagemin = require("gulp-imagemin");
const cfg = require("../config").images;

function images(cb) {
  return src(cfg.sourcePath)
    .pipe(imagemin(cfg.settings))
    .pipe(dest(cfg.destinationPath));
  cb();
}

function imagesClean(cb) {
  del([`${cfg.destinationPath}/**`]);
  cb();
}

exports.images = images;
exports.imagesClean = imagesClean;
