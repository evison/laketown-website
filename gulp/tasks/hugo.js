const argv = require("yargs").boolean("p").argv;
const util = require("util");
const exec = util.promisify(require("child_process").exec);
const path = require("path");
const cfg = require("../config").hugo;

/**
 * @name hugo:build
 * @task builds the site via Hugo
 */

/**
 * Destination Path
 * @type {String}
 */
const dest = path.join(process.cwd(), cfg.destinationPath);

/**
 * Source Path
 * @type {String}
 */
const src = path.join(process.cwd(), cfg.sourcePath);

/**
 * Production Mode
 * if set, the site will be rendered without drafts and with the production URL set in `hugo/config.yaml`
 * @type {Boolean}
 */
const isProduction = argv.p;

/**
 * DevMode Config
 * @type {String}
 */
const devOpts = !isProduction
  ? `--buildDrafts=true --baseUrl="${cfg.devHost}:${cfg.port}/"`
  : "";

/**
 * Command that will be executed by `exec()`
 * @type {String}
 */
const command = `hugo -s ${src} -d ${dest} ${devOpts}`;

async function hugo(cb) {
  try {
    const { stdout, stderr } = await exec(command);
    console.log(stdout);
    console.log(stderr);
    cb();
  } catch (e) {
    console.error(e); // should contain code (exit code) and signal (that caused the termination).
  }
  const { stdout, stderr } = await exec(command);
  console.log(stdout);
  console.log(stderr);
}

exports.hugo = hugo;
