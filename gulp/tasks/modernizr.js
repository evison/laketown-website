const { src, dest } = require("gulp");
const modernizr = require("gulp-modernizr");
const uglify = require("gulp-uglify");
const del = require("del");
const cfg = require("../config");

exports.modernizrBuild = function (cb) {
  return src(cfg.scripts.bundles)
    .pipe(modernizr(cfg.modernizr.fileName, cfg.modernizr.settings))
    .pipe(uglify())
    .pipe(dest(cfg.modernizr.destinationPath));
  cb();
};

exports.modernizrClean = function (cb) {
  del([`${cfg.modernizr.destinationPath}/${cfg.modernizr.fileName}`]);
  cb();
};
