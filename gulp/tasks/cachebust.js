const { src, dest } = require("gulp");
const hashsrc = require("gulp-hash-src");

function cachebust(cb) {
  src(["./public/**/*.html"])
    .pipe(
      hashsrc({
        build_dir: "./public",
        src_path: "./public/static/styles",
        exts: [".css"],
      })
    )
    .pipe(
      hashsrc({
        build_dir: "./public",
        src_path: "./public/static/scripts",
        exts: [".js"],
      })
    )
    .pipe(dest("./public"));
  cb();
}

exports.cachebust = cachebust;
