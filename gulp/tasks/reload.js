const gutil = require("gulp-util");
const BrowserSync = require("../config").browserSync.instance;

function reload(cb) {
  gutil.log(
    gutil.colors.green("Static files changed. Reloading BrowserSync...")
  );
  BrowserSync.reload();
  cb();
}

exports.reload = reload;
