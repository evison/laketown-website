const argv = require("yargs").boolean("p").argv;
const { src, dest } = require("gulp");
const gulpif = require("gulp-if");
const gutil = require("gulp-util");
const named = require("vinyl-named");
const del = require("del");
const webpack = require("webpack");
const webpackStream = require("webpack-stream");
const browserSync = require("../config").browserSync.instance;
const cfg = require("../config").scripts;
const streamSize = require("../util/streamsize");
const path = require("path");

const isProduction = argv.p;

const productionPlugins = [
  new webpack.DefinePlugin({
    "process.env": {
      NODE_ENV: JSON.stringify("production"),
    },
  }),
  new webpack.optimize.UglifyJsPlugin({
    compress: {
      warnings: false,
    },
  }),
  new webpack.ProvidePlugin({
    $: "jquery",
    jQuery: "jquery",
    "window.jQuery": "jquery",
    tether: "tether",
    Tether: "tether",
    "window.Tether": "tether",
  }),
];

/**
 * Webpack Config
 * @type {Object}
 */
const webpackConfig = {
  devtool: !isProduction ? "source-map" : false,
  externals: cfg.externals,
  resolve: {
    modules: [
      path.resolve("./"),
      path.resolve("hugo/layouts/partials/components"),
      path.resolve("node_modules"),
    ],
  },
  module: {
    loaders: [
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules)/,
        loader: "babel-loader",
      },
    ],
  },
  plugins: isProduction ? productionPlugins : [],
};

function scripts(cb) {
  return src(cfg.bundles)
    .pipe(named())
    .pipe(webpackStream(webpackConfig, webpack))
    .on("error", function logError(error) {
      gutil.log(gutil.colors.red(error.message));
      this.emit("end");
    })
    .pipe(dest(cfg.destinationPath))
    .pipe(gulpif(isProduction, streamSize("JS")))
    .pipe(browserSync.stream({ match: "**/*.js" }));
  cb();
}

function scriptsClean(cb) {
  del([
    `${cfg.destinationPath}/**`,
    `!${cfg.destinationPath}`,
    `!${cfg.destinationPath}/vendor/**`,
  ]);
}

exports.scripts = scripts;
exports.scriptsClean = scriptsClean;
