const dotenv = require("dotenv");
const util = require("util");
const exec = util.promisify(require("child_process").exec);
dotenv.config();

const datoPreview = process.env.PREVIEW === "true" ? "--preview" : "";

async function dato(cb) {
  try {
    const { stdout, stderr } = await exec(`npx dato dump ${datoPreview}`);
    console.log(stdout);
    console.log(stderr);
    cb();
  } catch (e) {
    console.error(e); // should contain code (exit code) and signal (that caused the termination).
  }
}

exports.dato = dato;
