const { src, dest } = require("gulp");
const cfg = require("../config").copy;

function copy(cb) {
  const tasks = cfg.bundles.map((bundle) => {
    src(bundle.sourcePath).pipe(dest(bundle.destinationPath));
  });
  cb();
}

exports.copy = copy;
