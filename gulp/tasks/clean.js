const del = require("del");
const cfg = require("../config");

async function clean(cb) {
  await del([`${cfg.images.destinationPath}/**`]);
  await del([`${cfg.modernizr.destinationPath}/${cfg.modernizr.fileName}`]);
  await del([
    `${cfg.scripts.destinationPath}/**`,
    `!${cfg.scripts.destinationPath}`,
    `!${cfg.scripts.destinationPath}/vendor/**`,
  ]);
  await del([`${cfg.styles.destinationPath}/**`]);
  await del([cfg.svg.destinationPath]);
  await del([cfg.destinationPath]);
  await del([cfg.hugoPostsPath, cfg.hugoDataPath]);
  cb();
}

exports.clean = clean;
