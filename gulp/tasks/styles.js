const argv = require("yargs").boolean("p").argv;
const autoprefixer = require("autoprefixer");
const cssnano = require("cssnano");
const del = require("del");
const { src, dest } = require("gulp");
const gulpif = require("gulp-if");
const postcss = require("gulp-postcss");
const sass = require("gulp-sass");
const sourcemaps = require("gulp-sourcemaps");
const sassGlob = require("gulp-sass-glob");
const browserSync = require("../config").browserSync.instance;
const cfg = require("../config").styles;
const streamSize = require("../util/streamsize");
const sasslint = require("gulp-sass-lint");

/**
 * @name - styles:build
 * @task - compiles, prefixes & minfies SCSS-files
 */

/**
 * Production Mode
 * if set, the css output will be optimized
 * @type {Boolean}
 */
const isProduction = argv.p;

/**
 * Processors that will be passed to postcss
 * @type {Array}
 */
const processors = [autoprefixer(cfg.autoprefixer)];

const productionProcessors = [...processors, cssnano()];

function styles(cb) {
  return src(cfg.sourcePath)
    .pipe(sasslint({ configFile: cfg.sassLintConfig }))
    .pipe(sasslint.format())
    .pipe(gulpif(!isProduction, sourcemaps.init()))
    .pipe(sassGlob())
    .pipe(sass().on("error", sass.logError))
    .pipe(postcss([require("postcss-flexbugs-fixes")]))
    .pipe(postcss(isProduction ? productionProcessors : processors))
    .pipe(gulpif(!isProduction, sourcemaps.write("./maps")))
    .pipe(gulpif(isProduction, streamSize("CSS")))
    .pipe(dest(cfg.destinationPath))
    .pipe(browserSync.stream({ match: "**/*.css" }));
  cb();
}

function stylesClean(cb) {
  del([`${cfg.destinationPath}/**`]);
  cb();
}

exports.styles = styles;
exports.stylesClean = stylesClean;
