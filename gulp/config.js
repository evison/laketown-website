/**
 * config.js
 * @exports {Object} - Configuration for Gulp Tasks
 */
const BrowserSync = require("browser-sync");

/**
 * Sets the target path for the build
 * @type {String}
 */
const destinationPath = "public";

/**
 * Sets the target path for static assets (scripts, styles, images, svg)
 * @type {String}
 */
const assetPath = `${destinationPath}/static`;

/**
 * Source path of the site template
 * @type {String}
 */
const sourcePath = "global";

/**
 * Host that BrowserSync serves from
 * @type {String}
 */
const host = "localhost";

/**
 * Port that BrowserSync serves from
 * @type {Number}
 */
const port = process.env.PORT || 3000;

/**
 * BrowserSync Config
 * `options` will be passed to BrowserSync instance
 * task: `tasks/watch.js` (other tasks may use browserSync instance)
 * @type {Object}
 */
const browserSync = {
  instance: BrowserSync.create(),
  settings: {
    host,
    notify: false,
    port,
    watchEvents: ["add", "change", "unlink", "addDir", "unlinkDir"],
    server: {
      baseDir: destinationPath,
    },
    online: true,
  },
};

/**
 * Copy Config
 * each bundle should be set up as an object with `sourcePath` & `destinationPath`. Globbing supported
 * task: `tasks/copy.js`
 * @type {Object}
 */
const copy = {
  // each bundle is an object consisting of source-glob and destination-path
  bundles: [
    {
      sourcePath: `${sourcePath}/webfonts/*.{ttf,woff,woff2}`,
      destinationPath: `${assetPath}/styles/webfonts`,
    },
    {
      sourcePath: `${sourcePath}/*.*`,
      destinationPath: assetPath,
    },
    {
      sourcePath: `${sourcePath}/videos/*.*`,
      destinationPath: `${assetPath}/videos`,
    },
    {
      sourcePath: `${sourcePath}/webfonts/*.*`,
      destinationPath: `${assetPath}/webfonts`,
    },
    {
      sourcePath: `${sourcePath}/_*`,
      destinationPath: `${destinationPath}/`,
    },
  ],
};

/**
 * Hugo Config
 * Task: `tasks/hugo.js`
 * @type {Object}
 */
const hugo = {
  devHost: `http://${host}`,
  destinationPath,
  sourcePath: "hugo",
  port,
  watch: [
    "hugo/content/**/*.md",
    "hugo/data/**/*",
    "hugo/config.yaml",
    "hugo/layouts/**/*.html",
  ],
};

/**
 * Images Config
 * `options` will be passed to imagemin
 * Task: `tasks/images.js`
 * @type {Object}
 */
const pngquant = require("imagemin-pngquant");
const jpegtran = require("imagemin-jpegtran");
const gifsicle = require("imagemin-gifsicle");
const images = {
  sourcePath: `${sourcePath}/images/**/*.{jpg,jpeg,png,gif,svg}`,
  destinationPath: `${assetPath}/images`,
  settings: {
    progressive: true,
    svgoPlugins: [{ removeViewBox: false }],
    use: [pngquant(), jpegtran(), gifsicle()],
  },
};

/**
 * Modernizr Settings
 * `tests` specify the modernizr tests to run
 * `settings` will be passed to customizr
 * task: `tasks/modernizr.js`
 * @type {Object}
 */
const modernizr = {
  destinationPath: `${assetPath}/scripts/vendor`,
  fileName: "modernizr-custom.js",
  settings: {
    classPrefix: "",
    minify: true,
    options: [
      "addTest",
      "html5shiv",
      "html5printshiv",
      "prefixedCSS",
      "setClasses",
    ],
    tests: [
      "touchevents",
      "geolocation",
      "css/boxshadow",
      "css/rgba",
      "css/transforms",
      "css/transforms3d",
      "css/transitions",
      "css/filters",
      "css/checked",
      "css/mask",
    ],
  },
};

/**
 * Scripts Config
 * uses webpack fur bundling. use the task to configure webpack
 * task: `tasks/scripts.js`
 * @type {Object}
 */
const scripts = {
  bundles: `${sourcePath}/scripts/**/*.js`,
  destinationPath: `${assetPath}/scripts`,
  components: "hugo/layouts/partials/**/*.js",
  externals: {
    // jquery: 'jQuery',
  },
};

/**
 * Styles Config
 * `autoprefixer` will be passed to autoprefixer
 * task: `tasks/styles.js`
 * @type {Object}
 */
const styles = {
  sourcePath: `${sourcePath}/styles/*.{sass,scss}`,
  destinationPath: `${assetPath}/styles`,
  watch: [
    `${sourcePath}/styles/**/*.{sass,scss}`,
    "hugo/layouts/partials/**/*.{sass,scss}",
  ],
  autoprefixer: {
    browsers: ["last 2 versions", "Safari >= 8"],
  },
  sassLintConfig: ".sass-lint.yml",
};

/**
 * SVG Config
 * task: `tasks/svg.js`
 * @type {Object}
 */
const svg = {
  sourcePath: `${sourcePath}/images/**/*.svg`,
  destinationPath: `${assetPath}/svg`,
};

const hugoPostsPath = "hugo/content/posts";
const hugoDataPath = "hugo/data";

const dato = {
  watch: "dato.config.js",
};

module.exports = {
  browserSync,
  copy,
  destinationPath,
  hugoPostsPath,
  hugoDataPath,
  dato,
  hugo,
  images,
  modernizr,
  port,
  scripts,
  styles,
  svg,
};
