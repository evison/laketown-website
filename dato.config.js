const util = require("util");
const DB_NAME = process.env.ENV_DB;

module.exports = (dato, root) => {


  root.directory("hugo/content", (dir) => {
    console.log("\n\nWriting data to hugo files...\n");
    console.log('deleted hugo/content')

    const events = { mainEvents: [], miniEvents: [], homePageCards: [] };

    /*-----------------------------------------------------------------------------
      Main Events
    ------------------------------------------------------------------------------*/
    dato.mainEvents.forEach((event) => {
      const mainEvent = {
        src: event.image
          ? `${event.image.url()}?w=800&h=437&q=50&fit=crop&fm=webp`
          : null,
        alt: event.image && event.image.alt ? event.image.alt : null,
        label: event.dates ? event.dates : null,
        title: event.title ? event.title : null,
        buttons: [],
        includeInNav: !event.hideFromNav,
        order: event.homePageCardOrder ? event.homePageCardOrder : 999,
      };
      event.ctas.forEach((cta) => {
        mainEvent.buttons.push({
          title: cta.label,
          href: cta.href,
        });
      });
      events.mainEvents.push(mainEvent);
      events.homePageCards.push(mainEvent);
    });

    /*-----------------------------------------------------------------------------
      Mini Events
    ------------------------------------------------------------------------------*/
    dato.miniEvents.forEach((event) => {
      const eventId = event.slug.replace(/-/g, "");

      // Home page card
      const card = {
        src: event.homePageCardImage
          ? `${event.homePageCardImage.url()}?w=800&h=437&q=50&fit=crop&fm=webp`
          : null,
        alt:
          event.homePageCardImage && event.homePageCardImage.alt
            ? event.homePageCardImage.alt
            : null,
        label: event.dateLabel ? event.dateLabel : null,
        title: event.title ? event.title : null,
        order: event.homePageCardOrder ? event.homePageCardOrder : 999,
        buttons: [
          {
            title: "Info",
            href: `/${event.slug}`,
          },
        ],
      };
      if (event.ticketsUrl) {
        card.buttons.push({
          title: event.ticketsLabel,
          href: event.ticketsUrl,
        });
      }
      events.homePageCards.push(card);

      // Global events data (events.json)
      events.miniEvents.push({
        title: event.title,
        href: `/${event.slug}`,
      });

      // Maps
      const maps = [];
      event.maps.forEach((map) => {
        maps.push({ src: map.imagePath, title: map.title });
      });

      // Event page (yaml)
      dir.createPost(`${event.slug}.md`, "yaml", {
        frontmatter: {
          title: `${event.title} | Laketown Ranch`,
          type: "page",
          layout: "event",
          hero_image: event.heroImage
            ? `${event.heroImage.url()}?w=1920&h=1080&q=50&fit=crop&fm=webp`
            : null,
          suppress_hero: event.heroImage ? false : true,
          hero_heading: event.title,
          slug: event.slug,
          event_id: eventId,
          more_acts: event.moreActs,
          more_acts_text: event.moreActsText ? event.moreActsText : null,
          tickets_label: event.ticketsLabel ? event.ticketsLabel : null,
          tickets_url: event.ticketsUrl ? event.ticketsUrl : null,
          share_title: event.sharingTitle ? event.sharingTitle : null,
          share_url: event.sharingUrl ? event.sharingUrl : null,
          share_description: event.sharingDescription
            ? event.sharingDescription
            : null,
          share_image: event.sharingImage ? event.sharingImage.url() : null,
          cards: maps,
          shuttle_info: event.shuttleInfo ? event.shuttleInfo : null,
          misc_content: event.miscContent ? event.miscContent : null,
        },
        content: `${event.info}`,
      });

      // Lineup
      const days = [];
      event.days.forEach((datoDay) => {
        // For each day
        const day = {
          title: datoDay.title,
          lineupImage: datoDay.lineupImage
            ? `${datoDay.lineupImage.url()}?w=800&q=60&fm=jpeg&bg=f4f4f3`
            : null,
        };
        day.artists = [];

        // Add all artists on that day
        event.artists.forEach((artist) => {
          artist.days.forEach((artistDay) => {
            if (artistDay.title === day.title) {
              day.artists.push({
                title: artist.title ? artist.title : null,
                image: artist.image
                  ? `${artist.image.url()}?w=1200&h=800&q=50&fm=webp&fit=crop`
                  : null,
                link: artist.websiteUrl ? artist.websiteUrl : null,
              });
            }
          });
        });

        days.push(day);
      });

      root.createDataFile(`hugo/data/minievents/${eventId}.json`, "json", days);
    });

    events.homePageCards = events.homePageCards.sort((a, b) => {
      return a.order - b.order;
    });
    root.createDataFile("hugo/data/events.json", "json", events);
    
    /*-----------------------------------------------------------------------------
      Pages
    ------------------------------------------------------------------------------*/

    function parseBlocks(item) {
      let content = [];
      let output = '';
      switch(item.itemType.apiKey) {
        case 'structured_content_block':
          content.push('{{< block-structured-content >}}'+render(item.data)+'{{< /block-structured-content >}}');
          break;
        case 'markdown_block':
          content.push('{{< block-markdown >}}'+item.markdownContent+'{{< /block-markdown >}}');
          break;
        case 'html_block':
          content.push('{{< block-html >}}'+item.htmlContent+'{{< /block-html >}}');
          break;           
        case 'contact_form':
          content.push('{{< block-contact-form title="'+item.title+'" >}}'+item.content+'{{< /block-contact-form >}}');
          break;
        case 'maps_block':
          output = '{{< block-maps >}}';
          item.mapsBlockContent.forEach((map) => {
            output += '{{< map src="'+map.image.url()+'" title="'+map.title+'" >}}';
          });
          output += '{{< /block-maps >}}';
          content.push(output);
          break;
        case 'faq_block':
        case 'faq_block2':
          output = '{{< block-faqs >}}';
          item.faqEntries.forEach((faq) => {
            output += '{{< faq order="'+faq.position+'" question="'+faq.question+'" >}}'+faq.answer+'{{< /faq >}}';
          });
          output += '{{< /block-faqs >}}';
          content.push(output);
          break;
        case 'cards_block':
          output = '{{< block-cards id="'+item.id+'" >}}';
          item.cardsBlockContent.forEach((card) => {
            output += '{{< card title="'+card.title+'" href="'+card.url+'" ';
            output += 'image="'+card.image.url({ w: 1160, h: 773, fm: "jpg", fit: "crop", crop: "faces", q: 50, })+'" ';
            output += 'show_title="'+card.showTitle+'" full_width="'+card.fullWidth+'" show_button="'+card.showButton+'" button_cta="'+card.buttonCta+'" >}}';
          });  
          output += '{{< /block-cards >}}';
          content.push(output);
          break;
        case 'hero_block':
          if(item.image) content.push('{{< block-hero src="'+item.image.url()+'" >}}');
          break;
        case 'subscribe_block':
          content.push('{{< block-subscribe >}}');
          break;
        case 'acknowledgement_block':
          content.push('{{< block-acknowledgement >}}'+dato.misc.territoryAcknowledgement+'{{< /block-acknowledgement >}}');
          break;            
        case 'sponsors_block':
          content.push('{{< block-sponsors >}}');
          break;
        case 'gallery_block':
          content.push('{{< block-gallery >}}');
          break;          
        case 'news_feed_block':
          content.push('{{< block-newsfeed >}}');
          break; 
        case 'faq_block_old':
          content.push('{{< block-faq >}}');
          break;
        case 'lineup_block':
          content.push('{{< block-lineup >}}');
          break;
        case 'blog_index_block':
          content.push('{{< block-blog-index >}}');
          break;          
        case 'event_cards_block':
          content.push('{{< block-event-cards title="'+item.title+'" >}}');
          break;          
        case 'wrapper_block':
          output = '{{< block-wrapper background="'+item.background+'" space_vertical="'+item.spaceTopBottom+'" full_width="'+item.fullWidth+'">}}';
          item.wrapperContent.forEach((block) => {
            output += parseBlocks(block);
          });
          output += '{{< /block-wrapper >}}';
          content.push(output);
          break;
        default:
          break
      }
      return content.join('');
    }

    dato.pages.forEach((record) => {
      let content = null;
      if(record.contentTest.length > 0) {
        content = ['<div class="content">'];
        record.contentTest.forEach((item) => {
          content.push(parseBlocks(item));
        });
        content.push('</div>');
      }
      var page_slug = record.slug; 
      const data = {           
          frontmatter: {
            title: record.title,
            type: "page",
            layout: "page",
            hero_image: (record.heroImage ? record.heroImage.url() : false),
            hero_heading: record.heroHeading ? record.heroHeading : record.title,
            hero_map: record.displayHeroMap,
            app_promo: record.pageDisplayAppPromo,
            no_app_banner: !record.pageDisplayAppPromo,
            contact_form: record.pageShowContactForm,
            news_heading: 'Laketown News',
          },content: content ? content.join('').trim() : record.pageContent,
      };
      if(page_slug == '_index') {
        data.frontmatter.is_home = true;
      }
      dir.createPost(`${page_slug}.md`, "yaml", data);
    });

    /*-----------------------------------------------------------------------------
      Blog Posts
    ------------------------------------------------------------------------------*/

    dato.posts.forEach((post, index) => {
      dir.createPost(`posts/${post.slug}.md`, "yaml", {
        frontmatter: {
          title: `${post.title} | Laketown Ranch`,
          type: "post",
          layout: "post",
          card_image: post.coverImage
            ? `${post.coverImage.url()}?w=1260&h=630&q=50&fit=crop&fm=webp`
            : null,
          hero_image: post.coverImage
            ? `${post.coverImage.url()}?w=1920&h=1080&q=60&fit=crop&fm=webp`
            : null,
          hero_heading: post.title,
          excerpt: post.excerpt,
          slug: post.slug,
          card_tag: post.cardTag,
          date: post.date,
          share_title:
            post.seoSettings && post.seoSettings.title
              ? post.seoSettings.title
              : null,
          share_url: `https://laketownranch.com/posts/${post.slug}`,
          share_description:
            post.seoSettings && post.seoSettings.description
              ? post.seoSettings.description
              : null,
          share_image: post.coverImage
            ? `${post.coverImage.url()}?w=1200&h=630&q=60&fit=crop&fm=webp`
            : null,
        },
        content: post.content,
      });      
    });

    /*-----------------------------------------------------------------------------
      Homepage and General Settings
    ------------------------------------------------------------------------------*/
    /*
    let general = {
      nextEventDate: dato.misc.nextEventDate,
      android_mobile_app_url: dato.misc.androidMobileAppUrl,
      ios_mobile_app_url: dato.misc.iosMobileAppUrl,
      footer_content: dato.misc.footerContent,
    };
    root.createDataFile("hugo/data/general.yml", "yaml", general);
    */
    let params = {
      googleAnalytics: "UA-29627747-14",
      keywords: "Laketown Ranch, Cowichan Valley, BC, event facility, concert venue, music festival",
      description: "Western Canada's premier outdoor event facility.",
      footer: {
        legal: "Copyright © 2024 Wideglide Entertainment Ltd. | All rights reserved",
        credit: "Site designed and built by Luminate",
      },
      hero404: "/static/images/hero-about-us.jpg",
      heading404: "Error 404",
      content404: "Hmmm, looks like that page doesn't exist.",
      custom_css: dato.misc.customCss,
    }
    root.createDataFile("hugo/config/_default/params.yaml", "yaml", params);

    /*-----------------------------------------------------------------------------
      Menus
    ------------------------------------------------------------------------------*/
    let menus = [];
    dato.menus.forEach((record) => {
      if(!record.entity.parentId) { 
        menus.push({ Name: record.text, URL: record.url, Params: { Target: record.target ? '_blank' : '' }, Weight: ( record.position + 1 ) });
        record.children.forEach((child) => {
          menus.push({ Name: child.text, URL: child.url, Params: { Target: child.target ? '_blank' : '' }, Parent: record.text, Weight: ( child.position + 1 ) });
        });
      }
    });
    root.createDataFile("hugo/config/_default/menus.yaml", "yaml", {main: menus});
  });
};
