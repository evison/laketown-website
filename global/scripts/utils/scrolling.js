import $ from 'jquery';

/**
 * Disables scrolling for the body. Calculates width of scrollbars for browser and
 * adjusts css to prevent content from "shifting over" when scrollbars are removed.
 * Sets up touch event handlers to fix some issues with iOS still allowing
 * scrolling for body when overlay is present.
 *
 * @param $overlay The modal/overlay that should be scrollable while body is not scrollable.
 */
export function disable($overlay, $scrollableContainer) {
  this.$body = $('body');
  this.$header = $('.js-header');

  // The overlay may not be the actual scrollable container, but if it is we
  // need to initialize it properly.
  $scrollableContainer = $scrollableContainer || $overlay;

  // If the body is overflowing, then calculate the width of scrollbars and add css
  const isBodyOverflowing = document.body.clientWidth < window.innerWidth;
  if (isBodyOverflowing) {
    // Need to calculate width of scrollbar to prevent shifting content
    // Thnx David Walsh
    const scrollDiv = document.createElement('div');
    scrollDiv.className = 'scrollbar-measure';
    document.body.appendChild(scrollDiv);
    const scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;
    document.body.removeChild(scrollDiv);

    // Apply css to header & body
    this.$header.css({ width: `calc(100% - ${scrollbarWidth}px)` });
    this.$body.css({ 'padding-right': scrollbarWidth });
  }

  // Disable scrolling for body via utility class
  this.$body.addClass('is-scroll-disabled');

  // Block some touch events to fix iOS issues
  // http://stackoverflow.com/questions/41594997/ios-10-safari-prevent-scrolling-behind-a-fixed-overlay-and-maintain-scroll-posi
  this.touchStartY = null;
  $overlay.on('touchstart.touchblocker', (e) => {
    if (e.targetTouches.length === 1) {
      // Save y scroll position on touch start
      this.touchStartY = e.targetTouches[0].clientY;
    }
  });

  // Block touch move if overlay is scrolled to top or bottom
  $overlay.on('touchmove.touchblocker', (e) => {
    if (e.targetTouches.length === 1) {
      const curTouchY = e.targetTouches[0].clientY - this.touchStartY;
      const scrollY = $scrollableContainer.scrollTop();

      // If we are not trying to scroll the scrollable are, disable scrolling
      if ($scrollableContainer[0] !== $overlay[0] && !$.contains($scrollableContainer[0], e.targetTouches[0].target)) {
        e.preventDefault();
      }

      // If overlay is scrolled to top
      if (scrollY === 0 && curTouchY > 0) {
        e.preventDefault();
      }

      // If overlay is scrolled to bottom
      if ($scrollableContainer[0].scrollHeight - scrollY <= $scrollableContainer[0].clientHeight && curTouchY < 0) {
        e.preventDefault();
      }
    }
  });
}

/**
 * Re-enables scrolling for the body.
 *
 * @param $overlay  The modal/overlay that should be scrollable while body is not scrollable.
 */
export function enable($overlay) {
  this.$body = $('body');
  this.$header = $('.js-header');

  // Reset inline css
  this.$header.css({ width: '' });
  this.$body.css({ 'padding-right': '' });

  // Remove class from body
  this.$body.removeClass('is-scroll-disabled');

  // Remove touch events from overlay
  $overlay.off('.touchblocker');
}
