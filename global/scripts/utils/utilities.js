import $ from 'jquery';

/**
 * Utility function for determining current breakpoint.
 * @returns {*}
 */
export function currentBreakpoint() {
  if (window.innerWidth >= 1800) {
    return {index: 6, string: 'extra-wide'};
  } else if (window.innerWidth >= 1300) {
    return {index: 5, string: 'wide'};
  } else if (window.innerWidth >= 980) {
    return {index: 4, string: 'desktop'};
  } else if (window.innerWidth >= 740) {
    return {index: 3, string: 'tablet'};
  } else if (window.innerWidth >= 530) {
    return {index: 2, string: 'phablet'};
  } else if (window.innerWidth >= 420) {
    return {index: 1, string: 'mobile-large'};
  } else {
    return {index: 0, string: 'mobile'};
  }
}

/**
 * Detects if video autoplay is supported
 * @returns {boolean}
 */
export function autoplaySupported() {
  const v = document.createElement('video');
  v.autoplay = true;
  v.muted = true;
  v.playsinline = true;
  v.play();

  let supported = !v.paused;

  var match = (navigator.appVersion).match(/OS (\d+)_(\d+)_?(\d+)?/),
    version;

  if (match !== undefined && match !== null) {
    version = [
      parseInt(match[1], 10),
      parseInt(match[2], 10),
      parseInt(match[3] || 0, 10)
    ];
    if (parseFloat(version.join('.')) >= 10) {
      supported = true;
    }
  }

  return supported;
}


/**
 * A JavaScript version of the hover-list effect (i.e., on hover of a list item,
 * fade other items to 60% opacity).
 * @param selector The css selector for the list items
 */
export function hoverList(selector) {
  const $elements = $(selector);
  $elements.on('mouseenter mouseleave', (e) => {
    $elements.not($(e.currentTarget)).stop(true).fadeTo(0, e.type === 'mouseenter' ? 0.75 : 1);
  });
}


/**
 * Helper function for normalizing the heights of repeated items within a list
 * of items. For example, the card list has several cards, and so this function
 * can be used to normalize the heights of the headings (so heading of each card
 * has the height equal to max heading height).
 * @param $lists
 * @param itemSelectors
 * @param reset If true, then resets the height of items
 * @param rowDependent If true, then picks max height only for items in the same row
 */
export function normalizeHeights($lists, itemSelectors, reset = false, rowDependent = true) {
  // If reset is true, then reset the heights of all elements
  if (reset) {
    if (!Array.isArray(itemSelectors)) {
      itemSelectors = [itemSelectors];
    }
    itemSelectors.forEach((itemSelector) => {
      $(itemSelector).height('');
    });
  } else {
    // Loop through each list (in case there's multiple on the page)
    $lists.each((index, list) => {
      const $list = $(list);
      const maxHeights = {};

      // Array of item selectors (may want to normalize the heights of multiple elements at once)
      // If only one passed, then convert to array
      if (!Array.isArray(itemSelectors)) {
        itemSelectors = [itemSelectors];
      }

      // Loop through of the item selectors
      itemSelectors.forEach((itemSelector) => {
        // Check if any instances of the item exist
        const $item = $list.find(itemSelector);
        if ($item.length) {
          const id = $item[0].outerHTML; // Use the outerHTML as an "ID"

          // Loop through each instance of item to get max height
          $item.each((itemsIndex, el) => {
            // We only want to normalize the heights of items that are in the same row.
            // As such, we can use the offset().top of the item to figure out which are
            // in the same row. We then find the max height for each row. However, if
            // rowDependent is false, then we ignore the offset position to when
            // creating the key. NOTE: We are rounding to the nearest 100px for the offset.
            const $el = $(el);
            const elPos = rowDependent ? Math.ceil($el.offset().top / 100) * 100 : 0;
            const key = `${id}-${elPos}`; // Each item/row combo has a different key
            $el.css('height', ''); // Reset elements inline height
            if (maxHeights[key] === undefined) {
              // If no max height set yet, set max height to height of the item
              maxHeights[key] = $el.height();
            } else {
              // Otherwise set to be max of current max height vs height of current item
              maxHeights[key] = Math.max(maxHeights[key], $el.height());
            }
          });

          // Loop through again to normalize items to max height
          $item.each((itemsIndex, el) => {
            const $el = $(el);
            const elPos = rowDependent ? Math.ceil($el.offset().top / 100) * 100 : 0;
            const key = `${id}-${elPos}`;
            $el.css('height', maxHeights[key]);
          });
        }
      });
    });
  }
}
