/**
 * main.js
 * Entrypoint for webpack
 */
import { ready, debug } from "./utils";

// Vendors
import "global/scripts/vendors/modernizr.3.6.0";
import "jquery.easing/jquery.easing";
import "global/scripts/vendors/bootstrap";
import "headroom.js/dist/headroom";
import "picturefill/dist/picturefill";

// Atoms
import "../../hugo/layouts/partials/components/atoms/hamburger-btn/hamburger-btn";
import "../../hugo/layouts/partials/components/atoms/external-links/external-links";
import "../../hugo/layouts/partials/components/atoms/collapse/collapse";
import "../../hugo/layouts/partials/components/atoms/juicer/juicer";
import "../../hugo/layouts/partials/components/atoms/hero-cta/hero-cta";
import "../../hugo/layouts/partials/components/atoms/_featherlight/featherlight";

// Molecules
import "../../hugo/layouts/partials/components/molecules/loader/loader";
import "../../hugo/layouts/partials/components/molecules/hero-video/hero-video";
import "../../hugo/layouts/partials/components/molecules/hero-image/hero-image";
import "../../hugo/layouts/partials/components/molecules/hero-map/hero-map";

// Organisms
import "../../hugo/layouts/partials/components/organisms/card-list/card-list";
import "../../hugo/layouts/partials/components/organisms/header/header";

function onReady(e) {
  debug("info", `Event: ${e.type}`, `Datestamp: ${this.date}`);
}

ready(onReady, {
  date: new Date(),
});
